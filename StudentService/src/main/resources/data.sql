INSERT INTO administrator (address, archived, lastname, mobile_phone, name, password, role, username)
VALUES
('123 Elm Street, NY', 0, 'Vujicic', 80665537, 'Adrijan', '$2a$10$CQAR8eI2nVs.GY7xqUvU/u1Sy1sJ8dTO1udwwHPTu7IWpPglY4HdK', 2, 'tt@mail.com'),
('55th Johnson Street, LA', 0, 'Jackson', 823455534, 'Alex', '$2a$10$CQAR8eI2nVs.GY7xqUvU/u1Sy1sJ8dTO1udwwHPTu7IWpPglY4HdK', 2, 'alexj@example.com'),
('3rd Red Apple st. , MI', 0, 'Doe', 840958904, 'Steven', '$2a$10$CQAR8eI2nVs.GY7xqUvU/u1Sy1sJ8dTO1udwwHPTu7IWpPglY4HdK', 2, 'steven.doe@example.com');

INSERT INTO student (address, archived, lastname, mobile_phone, name, password, role, username, account_balance, index_number)
VALUES
('10th Yellow st, SF', 0, 'Vujicic', 923847512, 'Adrijan', '$2a$10$ieCKCH1zVlWZDYnyDDMvU.9gJTQeILZUzqi65cF4osRF5CtFE91yi', 0, 'tt@mail.com',0,239530181),
('9th Sunset Boulevard, SF', 0, 'Doe', 923847512, 'John', '$2a$10$ieCKCH1zVlWZDYnyDDMvU.9gJTQeILZUzqi65cF4osRF5CtFE91yi', 0, 'john.doe@example.com',0,879345563),
('12th Maple Avenue, TX', 0, 'Smith', 834729054, 'Emily', '$2a$10$ieCKCH1zVlWZDYnyDDMvU.9gJTQeILZUzqi65cF4osRF5CtFE91yi', 0, 'emily.smith@example.com',0,248596737),
('6th Pine Lane, FL', 0, 'Miller', 758392641, 'Michael', '$2a$10$ieCKCH1zVlWZDYnyDDMvU.9gJTQeILZUzqi65cF4osRF5CtFE91yi', 0, 'michael.miller@example.com',0,475869784),
('21st Oakwood Drive, CO', 0, 'Johnson', 876509432, 'Olivia', '$2a$10$ieCKCH1zVlWZDYnyDDMvU.9gJTQeILZUzqi65cF4osRF5CtFE91yi', 0, 'olivia.johnson@example.com',0,374859684),
('88th Cedar Street, WA', 0, 'Brown', 940582738, 'James', '$2a$10$ieCKCH1zVlWZDYnyDDMvU.9gJTQeILZUzqi65cF4osRF5CtFE91yi', 0, 'james.brown@example.com',0,263748596),
('17th Birch Road, NV', 0, 'Taylor', 809567432, 'Sophia', '$2a$10$ieCKCH1zVlWZDYnyDDMvU.9gJTQeILZUzqi65cF4osRF5CtFE91yi', 0, 'sophia.taylor@example.com',0,736647859),
('42nd Elm Drive, NY', 0, 'Wilson', 762304958, 'David', '$2a$10$ieCKCH1zVlWZDYnyDDMvU.9gJTQeILZUzqi65cF4osRF5CtFE91yi', 0, 'david.wilson@example.com',0,142536425),
('15th Redwood Boulevard, OR', 0, 'Davis', 890473615, 'Charlotte', '$2a$10$ieCKCH1zVlWZDYnyDDMvU.9gJTQeILZUzqi65cF4osRF5CtFE91yi', 0, 'charlotte.davis@example.com',0,364758374),
('24th Palm Avenue, AZ', 0, 'Anderson', 840926375, 'Lucas', '$2a$10$ieCKCH1zVlWZDYnyDDMvU.9gJTQeILZUzqi65cF4osRF5CtFE91yi', 0, 'lucas.anderson@example.com',0,556473846),
('30th Cypress Street, IL', 0, 'Martin', 784509632, 'Ava', '$2a$10$ieCKCH1zVlWZDYnyDDMvU.9gJTQeILZUzqi65cF4osRF5CtFE91yi', 0, 'ava.martin@example.com',0,475837465);

INSERT INTO teacher (address, archived, lastname, mobile_phone, name, password, role, teacher_role, username)
VALUES
('High Road 43, NY', 0, 'Vujicic', 80665537, 'Adrijan', '$2a$10$CQAR8eI2nVs.GY7xqUvU/u1Sy1sJ8dTO1udwwHPTu7IWpPglY4HdK', 1, 2, 'tt@mail.com'),
('12 Greenfield Avenue, NY', 0, 'Brown', 834455564, 'Sarah', '$2a$10$CQAR8eI2nVs.GY7xqUvU/u1Sy1sJ8dTO1udwwHPTu7IWpPglY4HdK', 1, 2, 'sarah.brown@example.com'),
('78 Oakridge Drive, TX', 0, 'Johnson', 865677431, 'Michael', '$2a$10$CQAR8eI2nVs.GY7xqUvU/u1Sy1sJ8dTO1udwwHPTu7IWpPglY4HdK', 1, 2, 'm.johnson@example.com'),
('9 Riverside Lane, FL', 0, 'Davis', 802377198, 'Emma', '$2a$10$CQAR8eI2nVs.GY7xqUvU/u1Sy1sJ8dTO1udwwHPTu7IWpPglY4HdK', 1, 2, 'emma.davis@example.com'),
('44 Sunset Boulevard, CA', 0, 'Taylor', 892334512, 'James', '$2a$10$CQAR8eI2nVs.GY7xqUvU/u1Sy1sJ8dTO1udwwHPTu7IWpPglY4HdK', 1, 2, 'j.taylor@example.com'),
('63 Highland Road, IL', 0, 'Clark', 856477893, 'Olivia', '$2a$10$CQAR8eI2nVs.GY7xqUvU/u1Sy1sJ8dTO1udwwHPTu7IWpPglY4HdK', 1, 2, 'olivia.clark@example.com');

INSERT INTO subject (archived, ects, name)
VALUES
(0, 8, 'Environmental Science'),
(0, 12, 'Organic Chemistry'),
(0, 7, 'Principles of Economics'),
(0, 9, 'Modern European History'),
(0, 8, 'Introduction to Algorithms');

INSERT INTO subject_teachers (subjects_id, teachers_id)
VALUES
( 3, 2),
( 5, 3),
( 2, 4);

INSERT INTO subject_students (subjects_id, students_id)
VALUES
( 3, 2),
( 3, 3),
( 3, 4),
( 2, 5),
( 2, 6),
( 2, 7),
( 5, 8),
( 5, 9),
( 5, 10),
( 5, 11);

INSERT INTO exam (archived, date_time, name, subject_id, teacher_id)
VALUES
(0, '2024-11-18 23:00:00.000000', 'Macroeconomics Exam 1', 3, 2),
(0, '2024-11-18 23:00:00.000000', 'Macroeconomics Exam 2', 3, 2),
(0, '2024-11-18 23:00:00.000000', 'Inorganic Chemistry test', 2, 4),
(0, '2024-11-18 23:00:00.000000', 'Physical Chemistry test', 2, 4),
(0, '2024-11-18 23:00:00.000000', 'Data Structures exam', 5, 3),
(0, '2024-11-18 23:00:00.000000', 'Software Engineering exam', 5, 3);

