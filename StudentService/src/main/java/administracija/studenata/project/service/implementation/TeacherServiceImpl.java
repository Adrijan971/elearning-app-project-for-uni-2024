package administracija.studenata.project.service.implementation;

import administracija.studenata.project.controller.StudentController;
import administracija.studenata.project.dto.*;
import administracija.studenata.project.enums.Role;
import administracija.studenata.project.model.*;
import administracija.studenata.project.repository.TeacherRepository;
import administracija.studenata.project.service.ExamService;
import administracija.studenata.project.service.StudentService;
import administracija.studenata.project.service.TeacherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

//    @Autowired
//    ExamService examService;
//
//    @Autowired
//    StudentService studentService;

    @Override
    public List<Teacher> findAll() {

        List<Teacher> teacherList = teacherRepository.findAll();

        // Sort by id in ascending order
        teacherList.sort(Comparator.comparing(Teacher::getId));

        return teacherList;
    }

    @Override
    public Teacher findOneByName(String name) {

        Optional<Teacher> teacherOpt = teacherRepository.findFirstByName(name);

        if (teacherOpt.isPresent()) {
            return teacherOpt.get();
        }

        return null;
    }

    @Override
    public Teacher findOneById(Long id) {

        Optional<Teacher> teacherOpt = teacherRepository.findById(id);

        if (teacherOpt.isPresent()) {
            return teacherOpt.get();
        }

        return null;
    }

    @Override
    public Teacher createTeacher(TeacherDTO teacherDTO) {
        Teacher teacher = new Teacher(teacherDTO);

        return teacherRepository.save(teacher);
    }

    @Override
    public Teacher updateTeacher(TeacherDTO teacherDTO, Long id) {

        Teacher teacher = this.findOneById(id);

        if (teacher == null){
            return null;
        }

        if (teacherDTO.getName() != null){
            teacher.setName(teacherDTO.getName());
        }
        if (teacherDTO.getLastname() != null){
            teacher.setLastname(teacherDTO.getLastname());
        }
        if (teacherDTO.getUsername() != null){
            teacher.setUsername(teacherDTO.getUsername());
        }
        if (teacherDTO.getAddress() != null){
            teacher.setAddress(teacherDTO.getAddress());
        }
        if (teacherDTO.getMobilePhone() != 0){
            teacher.setMobilePhone(teacherDTO.getMobilePhone());
        }

        return teacherRepository.save(teacher);
    }

    @Override
    public Teacher archiveTeacher(Long id) {

        Teacher teacher = this.findOneById(id);

        if (teacher == null){
            return null;
        }

        teacher.setArchived(true);

        return teacherRepository.save(teacher);
    }

    public Teacher register(SignUpDTO signUpDTO){

        Optional<Teacher> optionalTeacher = teacherRepository.findFirstByUsername(signUpDTO.username());

        if (optionalTeacher.isPresent()) {
            throw new IllegalStateException("User with username " + signUpDTO.username() + " already exists.");
        }

        if (!signUpDTO.userType().equals(Role.TEACHER)) {
            throw new IllegalStateException("Wrong user type.");
        }

        Teacher teacher = new Teacher();
        teacher.setName(signUpDTO.firstName());
        teacher.setLastname(signUpDTO.lastName());
        teacher.setUsername(signUpDTO.username());
        teacher.setRole(signUpDTO.userType());
        teacher.setTeacherRole(signUpDTO.teacherType());
        teacher.setPassword(passwordEncoder.encode(
                new String(signUpDTO.password())
        ));

        return teacherRepository.save(teacher);
    }

    public TeacherDTO login(CredentialsDTO credentialsDto) {

        Optional<Teacher> optionalTeacher = teacherRepository.findFirstByUsername(credentialsDto.username());

        if (!optionalTeacher.isPresent()) {
            throw new IllegalStateException("User with username " + credentialsDto.username() + " does not exist.");
        }
        if(optionalTeacher.get().isArchived()){
            throw new IllegalStateException("User: " + credentialsDto.username() + " is archived.");
        }
        if (passwordEncoder.matches(new String(credentialsDto.password()), optionalTeacher.get().getPassword())) {
            return new TeacherDTO(optionalTeacher.get());
        }
        else{
            throw new IllegalStateException("Password of user: " + credentialsDto.username() + " does not match.");
        }
    }

    @Override
    public Teacher save(Teacher teacher) {
        return teacherRepository.save(teacher);
    }

}
