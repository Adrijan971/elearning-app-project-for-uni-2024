package administracija.studenata.project.service.implementation;

import administracija.studenata.project.dto.DocumentDTO;
import administracija.studenata.project.dto.DocumentDTOWithNames;
import administracija.studenata.project.dto.ExamDTOWithNames;
import administracija.studenata.project.model.Administrator;
import administracija.studenata.project.model.Document;
import administracija.studenata.project.model.Exam;
import administracija.studenata.project.model.Student;
import administracija.studenata.project.repository.DocumentRepository;
import administracija.studenata.project.service.DocumentService;
import administracija.studenata.project.service.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class DocumentServiceImpl implements DocumentService {

    @Value("${upload.directory}")
    private String uploadDirectory; // This will point to external directory

    private static final Logger logger = LoggerFactory.getLogger(DocumentService.class);

    @Autowired
    DocumentRepository documentRepository;

    @Autowired
    StudentService studentService;

    @Override
    public List<Document> findAll() {

        List<Document> documentList = documentRepository.findAll();

        // Sort by id in ascending order
        documentList.sort(Comparator.comparing(Document::getId));

        return documentList;
    }

    @Override
    public List<DocumentDTOWithNames> findAllWithNames() {

        List<Document> documents = documentRepository.findAll();

        List<DocumentDTOWithNames> documentDTOs = new ArrayList<>();

        for (Document document : documents) {

            String studentName = studentService.findOneById(document.getStudent().getId()).getName();
            String studentLastName = studentService.findOneById(document.getStudent().getId()).getLastname();

            if(!document.isArchived()){
                DocumentDTOWithNames dto = new DocumentDTOWithNames();
                dto.setId(document.getId());
                dto.setName(document.getName());
                dto.setArchived(document.isArchived());
                dto.setStudent(studentName + " " + studentLastName);
                dto.setContent(document.getContent());
                documentDTOs.add(dto);
            }
        }

        // Sort by id in ascending order
        documentDTOs.sort(Comparator.comparing(DocumentDTOWithNames::getId));

        return documentDTOs;
    }

    @Override
    public List<DocumentDTOWithNames> findAllPerStudentWithNames(Long id){
        List<Document> documents = this.documentRepository.findByStudentId(id);

        List<DocumentDTOWithNames> documentDTOs = new ArrayList<>();

        for (Document document : documents) {

            String studentName = studentService.findOneById(document.getStudent().getId()).getName();
            String studentLastName = studentService.findOneById(document.getStudent().getId()).getLastname();

            if(!document.isArchived()){
                DocumentDTOWithNames dto = new DocumentDTOWithNames();
                dto.setId(document.getId());
                dto.setName(document.getName());
                dto.setArchived(document.isArchived());
                dto.setStudent(studentName + " " + studentLastName);
                dto.setContent(document.getContent());
                documentDTOs.add(dto);
            }
        }

        // Sort by id in ascending order
        documentDTOs.sort(Comparator.comparing(DocumentDTOWithNames::getId));

        return documentDTOs;
    }

    @Override
    public Document findOneById(Long id) {

        Optional<Document> documentOpt = documentRepository.findById(id);

        if (documentOpt.isPresent()) {
            return documentOpt.get();
        }

        return null;
    }

    @Override
    public Document uploadDocument(MultipartFile file, String name, Long studentId) throws IOException {


        try {
            // Get the original file name
            String fileName = file.getOriginalFilename();
            Path filePath = Paths.get(uploadDirectory, fileName);

            // Ensure the directory exists where the file will be stored
            Files.createDirectories(filePath.getParent());

            // Check if the file already exists and delete it (if overwrite is allowed)
            if (Files.exists(filePath)) {
                logger.warn("File already exists. Overwriting: {}", filePath);
                Files.delete(filePath);  // Delete the existing file
            }

            // Save the file to the specified directory
            Files.copy(file.getInputStream(), filePath);

            // Create a Document entity and set the file details
            Document document = new Document();
            document.setName(name);
            document.setStudent(studentService.findOneById(studentId));
            // document.setDescription(description); // Uncomment if description is needed

            // Set the path to the file that will be served as a static resource (relative to the web root)
            document.setContent("/uploads/" + fileName);  // This path should match the resource handler

            // Save the document information to the database
            return documentRepository.save(document);

        } catch (IOException e) {
            // Log the error details and rethrow or handle the exception as needed
            logger.error("Error while uploading document: {}", e.getMessage());
            throw new RuntimeException("Failed to upload the document.", e);  // Optional, you can customize this part
        }
    }

    @Override
    public File getDocumentByName(String documentName) {
        // Create a File object for the document
        File file = new File(uploadDirectory + documentName);

        // Return the file if it exists
        if (file.exists()) {
            return file;
        }

        // Return null if the file is not found
        return null;
    }


    @Override
    public Document createDocument(DocumentDTO documentDTO) {

        Document document = new Document(documentDTO);

        if (documentDTO.getStudentId() != 0){
            Student student = studentService.findOneById(documentDTO.getStudentId());
            document.setStudent(student);
        }

        return documentRepository.save(document);
    }

    @Override
    public Document updateDocument(DocumentDTO documentDTO, Long id) {

        Document document = this.findOneById(id);

        if (document == null){
            return null;
        }

        if (documentDTO.getName() != null){
            document.setName(documentDTO.getName());
        }

        if (documentDTO.getContent() != null){
            document.setContent(documentDTO.getContent());
        }

        return documentRepository.save(document);

    }

    @Override
    public Document archiveDocument(Long id) {

        Document document = this.findOneById(id);

        if (document == null){
            return null;
        }

        document.setArchived(true);

        return documentRepository.save(document);
    }
}
