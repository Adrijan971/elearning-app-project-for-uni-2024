package administracija.studenata.project.service.implementation;

import administracija.studenata.project.dto.ExamDTO;
import administracija.studenata.project.dto.ExamDTOWithNames;
import administracija.studenata.project.dto.StudentExamDTO;
import administracija.studenata.project.dto.StudentExamDTOWithNames;
import administracija.studenata.project.model.Administrator;
import administracija.studenata.project.model.Exam;
import administracija.studenata.project.model.Student;
import administracija.studenata.project.model.StudentExam;
import administracija.studenata.project.repository.StudentExamRepository;
import administracija.studenata.project.service.ExamService;
import administracija.studenata.project.service.StudentExamService;
import administracija.studenata.project.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class StudentExamServiceImpl implements StudentExamService {

    @Autowired
    StudentExamRepository studentExamRepository;

    @Autowired
    StudentService studentService;

    @Autowired
    ExamService examService;

    @Override
    public List<StudentExam> findAll() {

        List<StudentExam> applicationsList = studentExamRepository.findAll();

        // Sort by id in ascending order
        applicationsList.sort(Comparator.comparing(StudentExam::getId));

        return applicationsList;
    }

    @Override
    public List<StudentExamDTOWithNames> findAllWithNames() {

        List<StudentExam> applications = this.studentExamRepository.findAll();
        List<StudentExamDTOWithNames> applicationDTOsWithNames = new ArrayList<>();

        for (StudentExam application : applications) {
            StudentExamDTOWithNames dto = new StudentExamDTOWithNames();
            dto.setId(application.getId());
            dto.setArchived(application.isArchived());
            dto.setPassed(application.isPassed());
            dto.setGrade(application.getGrade());
            dto.setApplicationDate(application.getApplicationDate());
            dto.setExam(examService.findOneById(application.getExam().getId()).getName());
            dto.setStudent(studentService.findOneById(application.getStudent().getId()).getName());
            applicationDTOsWithNames.add(dto);
        }

        // Sort by id in ascending order
        applicationDTOsWithNames.sort(Comparator.comparing(StudentExamDTOWithNames::getId));

        return applicationDTOsWithNames;
    }

    @Override
    public List<StudentExamDTOWithNames> findAllPerStudentWithNames(Long id) {

        List<StudentExam> applications = this.studentExamRepository.findByStudentId(id);
        List<StudentExamDTOWithNames> applicationDTOsWithNames = new ArrayList<>();

        for (StudentExam application : applications) {
            if(!application.isArchived()){
                StudentExamDTOWithNames dto = new StudentExamDTOWithNames();
                dto.setId(application.getId());
                dto.setArchived(application.isArchived());
                dto.setPassed(application.isPassed());
                dto.setGrade(application.getGrade());
                dto.setApplicationDate(application.getApplicationDate());
                dto.setExam(examService.findOneById(application.getExam().getId()).getName());
                dto.setStudent(studentService.findOneById(application.getStudent().getId()).getName());
                applicationDTOsWithNames.add(dto);
            }
        }

        // Sort by id in ascending order
        applicationDTOsWithNames.sort(Comparator.comparing(StudentExamDTOWithNames::getId));

        return applicationDTOsWithNames;
    }

    @Override
    public List<StudentExamDTOWithNames> findAllPerTeacherWithNames(Long id) {

        // uzeti listu examova po teacheru
        List<Exam> exams = examService.findAllPerTeacher(id);

        // za svaki exam uzeti listu aplikacija i mergovati u listu i vratiti je
        List<StudentExamDTOWithNames> applicationDTOsWithNames = new ArrayList<>();

        for (Exam exam : exams ){
            Set<StudentExam> applciations = exam.getApplications();
            for (StudentExam application : applciations ){
                StudentExamDTOWithNames dto = new StudentExamDTOWithNames();
                dto.setId(application.getId());
                dto.setArchived(application.isArchived());
                dto.setPassed(application.isPassed());
                dto.setGrade(application.getGrade());
                dto.setApplicationDate(application.getApplicationDate());
                dto.setExam(examService.findOneById(application.getExam().getId()).getName());
                dto.setStudent(studentService.findOneById(application.getStudent().getId()).getName());
                applicationDTOsWithNames.add(dto);
            }
        }

        // Sort by id in ascending order
        applicationDTOsWithNames.sort(Comparator.comparing(StudentExamDTOWithNames::getId));

        // *** proveriti sta ovo ispod radi, deluje mi kao visak

//        List<StudentExam> applications = this.studentExamRepository.findByStudentId(id);
//
//        for (StudentExam application : applications) {
//            StudentExamDTOWithNames dto = new StudentExamDTOWithNames();
//            dto.setId(application.getId());
//            dto.setArchived(application.isArchived());
//            dto.setPassed(application.isPassed());
//            dto.setApplicationDate(application.getApplicationDate());
//            dto.setExam(examService.findOneById(application.getExam().getId()).getName());
//            dto.setStudent(studentService.findOneById(application.getStudent().getId()).getName());
//            applicationDTOsWithNames.add(dto);
//        }

        return applicationDTOsWithNames;
    }

    @Override
    public List<StudentExamDTOWithNames> findAllPerExamWithNames(Long id) {

        List<StudentExam> applications = this.studentExamRepository.findByExamId(id);
        List<StudentExamDTOWithNames> applicationDTOsWithNames = new ArrayList<>();

        for (StudentExam application : applications) {
            if(!application.isArchived()){

                Student student = studentService.findOneById(application.getStudent().getId());

                StudentExamDTOWithNames dto = new StudentExamDTOWithNames();
                dto.setId(application.getId());
                dto.setArchived(application.isArchived());
                dto.setPassed(application.isPassed());
                dto.setGrade(application.getGrade());
                dto.setApplicationDate(application.getApplicationDate());
                dto.setExam(examService.findOneById(application.getExam().getId()).getName());
                dto.setStudent(student.getName()+" "+student.getLastname());
                applicationDTOsWithNames.add(dto);
            }
        }

        // Sort by id in ascending order
        applicationDTOsWithNames.sort(Comparator.comparing(StudentExamDTOWithNames::getId));

        return applicationDTOsWithNames;
    }


    @Override
    public StudentExam findOneById(Long id) {
        Optional<StudentExam> studentExamOpt = studentExamRepository.findById(id);

        if (studentExamOpt.isPresent()) {
            return studentExamOpt.get();
        }

        return null;
    }

    @Override
    public StudentExam createStudentExam(StudentExamDTO studentExamDTO) {

        StudentExam studentExam = new StudentExam(studentExamDTO);

        if (studentExamDTO.getExamId() != 0){
            Exam exam = examService.findOneById(studentExamDTO.getExamId());
            studentExam.setExam(exam);
        }
        if (studentExamDTO.getStudentId() != 0){
            Student student = studentService.findOneById(studentExamDTO.getStudentId());
            studentExam.setStudent(student);
        }

        if (studentService.deductMoney(studentExamDTO.getStudentId(), 200)){
            return studentExamRepository.save(studentExam);
        }
        else{
            throw new IllegalArgumentException("Amount must be greater than zero.");
        }
    }

    @Override
    public StudentExam updateStudentExam(StudentExamDTO studentExamDTO, Long id) {

        StudentExam studentExam = this.findOneById(id);

        if (studentExam == null){
            return null;
        }

        if (studentExamDTO.getApplicationDate() != null){
            studentExam.setApplicationDate(studentExamDTO.getApplicationDate());
        }

        return studentExamRepository.save(studentExam);
    }

    @Override
    public StudentExam archiveStudentExam(Long id) {

        StudentExam studentExam = this.findOneById(id);

        if (studentExam == null){
            return null;
        }

        studentExam.setArchived(true);

        return studentExamRepository.save(studentExam);
    }

    @Override
    public StudentExam passStudentExam(Long id, int grade) {

        StudentExam studentExam = this.findOneById(id);

        if (studentExam == null){
            return null;
        }

        if(grade < 6){
            studentExam.setPassed(false);
        }
        else{
            studentExam.setPassed(true);
        }

        studentExam.setGrade(grade);

        return studentExamRepository.save(studentExam);
    }


}
