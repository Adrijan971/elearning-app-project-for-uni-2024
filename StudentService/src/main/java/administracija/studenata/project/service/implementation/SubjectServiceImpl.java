package administracija.studenata.project.service.implementation;

import administracija.studenata.project.dto.SubjectDTO;
import administracija.studenata.project.model.Administrator;
import administracija.studenata.project.model.Student;
import administracija.studenata.project.model.Subject;
import administracija.studenata.project.repository.SubjectRepository;
import administracija.studenata.project.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class SubjectServiceImpl implements SubjectService {

    @Autowired
    SubjectRepository subjectRepository;

    @Override
    public List<Subject> findAll() {

        List<Subject> subjectList = subjectRepository.findAll();

        // Sort by id in ascending order
        subjectList.sort(Comparator.comparing(Subject::getId));

        return subjectList;
    }

    @Override
    public List<Subject> findAllByStudentId(Long id) {

        List<Subject> subjectList = subjectRepository.findSubjectsByStudentId(id);

        // Sort by id in ascending order
        subjectList.sort(Comparator.comparing(Subject::getId));

        return subjectList;
    }


    @Override
    public Subject findOneByName(String name) {

        Optional<Subject> subjectOpt = subjectRepository.findFirstByName(name);

        if (subjectOpt.isPresent()) {
            return subjectOpt.get();
        }

        return null;
    }


    @Override
    public Subject findOneById(Long id) {

        Optional<Subject> subjectOpt = subjectRepository.findById(id);

        if (subjectOpt.isPresent()) {
            return subjectOpt.get();
        }

        return null;
    }

    @Override
    public Subject createSubject(SubjectDTO subjectDTO) {

        Subject subject = new Subject(subjectDTO);

        return subjectRepository.save(subject);
    }

    @Override
    public Subject save(Subject subject) {
        return subjectRepository.save(subject);
    }

    @Override
    public Subject archiveSubject(Long id) {

        Subject subject = this.findOneById(id);

        if (subject == null){
            return null;
        }

        subject.setArchived(true);

        return subjectRepository.save(subject);
    }

    @Override
    public Subject updateSubject(SubjectDTO subjectDTO, Long id) {

        Subject subject = this.findOneById(id);

        if (subject == null){
            return null;
        }

        if (subjectDTO.getName() != null){
            subject.setName(subjectDTO.getName());
        }

        if (subjectDTO.getEcts() != 0){
            subject.setEcts(subjectDTO.getEcts());
        }

        return subjectRepository.save(subject);
    }
}
