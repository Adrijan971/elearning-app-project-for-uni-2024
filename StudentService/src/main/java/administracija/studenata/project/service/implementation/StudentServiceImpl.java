package administracija.studenata.project.service.implementation;

import administracija.studenata.project.dto.*;
import administracija.studenata.project.enums.Role;
import administracija.studenata.project.model.*;
import administracija.studenata.project.repository.StudentRepository;
import administracija.studenata.project.service.ExamService;
import administracija.studenata.project.service.StudentService;
import administracija.studenata.project.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    SubjectService subjectService;

    @Autowired
    ExamService examService;

    @Override
    public List<Student> findAll() {

        List<Student> studentList = studentRepository.findAll();

        // Sort by id in ascending order
        studentList.sort(Comparator.comparing(Student::getId));

        return studentList;
    }

    @Override
    public Student findOneByName(String name) {

        Optional<Student> studentOpt = studentRepository.findFirstByName(name);

        if (studentOpt.isPresent()) {
            return studentOpt.get();
        }

        return null;
    }

    @Override
    public Student findOneById(Long id) {

        Optional<Student> studentOpt = studentRepository.findById(id);

        if (studentOpt.isPresent()) {
            return studentOpt.get();
        }

        return null;
    }


    @Override
    public List<ExamDTOWithNames> findAllExamsPerStudentWithNames(Long id) {

        // uzeti listu subjecta po studentu
        List<Subject> subjects = subjectService.findAllByStudentId(id);

        // za svaki subject uzeti listu examova i mergovati u listu i vratiti je
        List<ExamDTOWithNames> examDTOsWithNames = new ArrayList<>();

        for (Subject subject : subjects ){
            Set<Exam> exams = subject.getExams();
            for (Exam exam : exams ){
                ExamDTOWithNames dto = new ExamDTOWithNames();
                dto.setId(exam.getId());
                dto.setDateTime(exam.getDateTime());
                dto.setName(exam.getName());
                dto.setArchived(exam.isArchived());
                dto.setSubjectName(examService.findOneById(exam.getId()).getSubject().getName());
                dto.setTeacherName(examService.findOneById(exam.getId()).getTeacher().getName());
                examDTOsWithNames.add(dto);
            }
        }

        // Sort by id in ascending order
        examDTOsWithNames.sort(Comparator.comparing(ExamDTOWithNames::getId));

        return examDTOsWithNames;
    }

    @Override
    public Student createStudent(StudentDTO studentDTO) {

        Student student = new Student(studentDTO);

        return studentRepository.save(student);
    }

    @Override
    public Student save(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Student updateStudent(StudentDTO studentDTO, Long id) {

        Student student = this.findOneById(id);

        if (student == null){
            return null;
        }

        if (studentDTO.getName() != null){
            student.setName(studentDTO.getName());
        }
        if (studentDTO.getLastname() != null){
            student.setLastname(studentDTO.getLastname());
        }
        if (studentDTO.getUsername() != null){
            student.setUsername(studentDTO.getUsername());
        }
        if (studentDTO.getAddress() != null){
            student.setAddress(studentDTO.getAddress());
        }
        if (studentDTO.getMobilePhone() != 0){
            student.setMobilePhone(studentDTO.getMobilePhone());
        }

        return studentRepository.save(student);
    }

    @Override
    public Student archiveStudent(Long id) {

        Student student = this.findOneById(id);

        if (student == null){
            return null;
        }

        student.setArchived(true);

        return studentRepository.save(student);

    }

    @Override
    public Student addMoney(Long id, double amount) {

        Student student = this.findOneById(id);

        if (student == null){
            return null;
        }

        student.setAccountBalance(student.getAccountBalance()+amount);

        return studentRepository.save(student);
    }

    @Override
    public boolean deductMoney(Long id, double amount) {

        Student student = this.findOneById(id);

        if (student.getAccountBalance() >= amount){
            student.setAccountBalance(student.getAccountBalance() - amount);
            return true;
        }
        else{
            return false;
        }
    }

    public StudentDTO login(CredentialsDTO credentialsDto) {

        Optional<Student> optionalStudent = studentRepository.findFirstByUsername(credentialsDto.username());

        if (!optionalStudent.isPresent()) {
            throw new IllegalStateException("User with username " + credentialsDto.username() + " does not exist.");
        }
        if(optionalStudent.get().isArchived()){
            throw new IllegalStateException("User: " + credentialsDto.username() + " is archived.");
        }
        if (passwordEncoder.matches(new String(credentialsDto.password()), optionalStudent.get().getPassword())) {
            return new StudentDTO(optionalStudent.get());
        }
        else{
            throw new IllegalStateException("Password of user: " + credentialsDto.username() + " does not match.");
        }
    }

    public Student register(SignUpDTO signUpDTO){

        Optional<Student> optionalStudent = studentRepository.findFirstByUsername(signUpDTO.username());

        if (optionalStudent.isPresent()) {
            throw new IllegalStateException("User with username " + signUpDTO.username() + " already exists.");
        }

        if (!signUpDTO.userType().equals(Role.STUDENT)) {
            throw new IllegalStateException("Wrong user type.");
        }

        int randomIndexNo = (int) (Math.random() * 1000000000);

        Student student = new Student();
        student.setIndexNumber(randomIndexNo);
        student.setName(signUpDTO.firstName());
        student.setLastname(signUpDTO.lastName());
        student.setUsername(signUpDTO.username());
        student.setRole(signUpDTO.userType());
        student.setPassword(passwordEncoder.encode(
                new String(signUpDTO.password())
        ));

        return studentRepository.save(student);
    }
}