package administracija.studenata.project.service;

import administracija.studenata.project.dto.ExamDTOWithNames;
import administracija.studenata.project.dto.StudentExamDTO;
import administracija.studenata.project.dto.StudentExamDTOWithNames;
import administracija.studenata.project.model.StudentExam;

import java.util.List;

public interface StudentExamService {

    List<StudentExam> findAll();

    List<StudentExamDTOWithNames> findAllWithNames();

    List<StudentExamDTOWithNames> findAllPerStudentWithNames(Long id);

    List<StudentExamDTOWithNames> findAllPerTeacherWithNames(Long id);

    List<StudentExamDTOWithNames> findAllPerExamWithNames(Long id);

    StudentExam findOneById(Long id);

    StudentExam createStudentExam(StudentExamDTO studentExamDTO);

    StudentExam updateStudentExam(StudentExamDTO studentExamDTO, Long id);

    StudentExam archiveStudentExam(Long id);

    StudentExam passStudentExam(Long id, int grade);

}
