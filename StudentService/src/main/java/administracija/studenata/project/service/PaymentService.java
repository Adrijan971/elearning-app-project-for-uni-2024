package administracija.studenata.project.service;

import administracija.studenata.project.dto.ExamDTOWithNames;
import administracija.studenata.project.dto.PaymentDTO;
import administracija.studenata.project.dto.PaymentDTOWithNames;
import administracija.studenata.project.model.Payment;

import java.util.List;

public interface PaymentService {

    List<Payment> findAll();

    List<PaymentDTOWithNames> findAllWithNames();

    List<PaymentDTOWithNames> findAllPerStudentWithNames(Long id);

    Payment findOneById(Long id);

    Payment createPayment(PaymentDTO paymentDTO);

    Payment updatePayment(PaymentDTO paymentDTO, Long id);

    Payment archivePayment(Long id);

}
