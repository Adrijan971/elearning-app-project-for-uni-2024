package administracija.studenata.project.service.implementation;

import administracija.studenata.project.dto.*;
import administracija.studenata.project.enums.Role;
import administracija.studenata.project.model.Administrator;
import administracija.studenata.project.model.Student;
import administracija.studenata.project.model.Teacher;
import administracija.studenata.project.repository.AdministratorRepository;
import administracija.studenata.project.repository.SubjectRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import administracija.studenata.project.service.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class AdministratorServiceImpl implements AdministratorService {

    @Autowired
    AdministratorRepository administratorRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public List<Administrator> findAll() {

        List<Administrator> adminList = administratorRepository.findAll();

        // Sort by id in ascending order
        adminList.sort(Comparator.comparing(Administrator::getId));

        return adminList;
    }

    @Override
    public Administrator findOneByName(String name) {

        Optional<Administrator> administratorOpt = administratorRepository.findFirstByName(name);

        if (administratorOpt.isPresent()) {
            return administratorOpt.get();
        }

        return null;
    }

    @Override
    public Administrator findOneById(Long id) {

        Optional<Administrator> administratorOpt = administratorRepository.findById(id);

        if (administratorOpt.isPresent()) {
            return administratorOpt.get();
        }

        return null;
    }

    @Override
    public Administrator createAdministrator(AdministratorDTO administratorDTO) {

        Administrator administrator = new Administrator(administratorDTO);

        return administratorRepository.save(administrator);
    }

    @Override
    public Administrator updateAdministrator(AdministratorDTO administratorDTO, Long id) {


        Administrator administrator = this.findOneById(id);

        if (administrator == null){
            return null;
        }

        if (administratorDTO.getName() != null){
            administrator.setName(administratorDTO.getName());
        }
        if (administratorDTO.getLastname() != null){
            administrator.setLastname(administratorDTO.getLastname());
        }
        if (administratorDTO.getUsername() != null){
            administrator.setUsername(administratorDTO.getUsername());
        }
        if (administratorDTO.getAddress() != null){
            administrator.setAddress(administratorDTO.getAddress());
        }
        if (administratorDTO.getMobilePhone() != 0){
            administrator.setMobilePhone(administratorDTO.getMobilePhone());
        }
        
        return administratorRepository.save(administrator);
    }

    @Override
    public Administrator archiveAdministrator(Long id) {

        Administrator administrator = this.findOneById(id);

        if (administrator == null){
            return null;
        }

        administrator.setArchived(true);

        return administratorRepository.save(administrator);
    }

    public Administrator register(SignUpDTO signUpDTO){
        System.out.println("TESTTESTTEST");
        Optional<Administrator> optionalAdministrator = administratorRepository.findFirstByUsername(signUpDTO.username());

        if (optionalAdministrator.isPresent()) {
            throw new IllegalStateException("User with username " + signUpDTO.username() + " already exists.");
        }

        System.out.println("role:");
        System.out.println(signUpDTO.userType());

        if (!signUpDTO.userType().equals(Role.ADMINISTRATOR)) {
            throw new IllegalStateException("Wrong user type.");
        }

        Administrator administrator = new Administrator();
        administrator.setName(signUpDTO.firstName());
        administrator.setLastname(signUpDTO.lastName());
        administrator.setUsername(signUpDTO.username());
        administrator.setRole(signUpDTO.userType());
        administrator.setPassword(passwordEncoder.encode(
                new String(signUpDTO.password())
        ));

        return administratorRepository.save(administrator);
    }

    public AdministratorDTO login(CredentialsDTO credentialsDto) {

        Optional<Administrator> optionalAdministrator = administratorRepository.findFirstByUsername(credentialsDto.username());

        if (!optionalAdministrator.isPresent()) {
            throw new IllegalStateException("User with username " + credentialsDto.username() + " does not exist.");
        }
        if(optionalAdministrator.get().isArchived()){
            throw new IllegalStateException("User: " + credentialsDto.username() + " is archived.");
        }
        if (passwordEncoder.matches(new String(credentialsDto.password()), optionalAdministrator.get().getPassword())) {
            return new AdministratorDTO(optionalAdministrator.get());
        }
        else{
            throw new IllegalStateException("Password of user: " + credentialsDto.username() + " does not match.");
        }
    }

}
