package administracija.studenata.project.service.implementation;

import administracija.studenata.project.dto.ExamDTO;
import administracija.studenata.project.dto.ExamDTOWithNames;
import administracija.studenata.project.model.*;
import administracija.studenata.project.repository.ExamRepository;
import administracija.studenata.project.repository.SubjectRepository;
import administracija.studenata.project.service.ExamService;
import administracija.studenata.project.service.SubjectService;
import administracija.studenata.project.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class ExamServiceImpl implements ExamService {

    @Autowired
    ExamRepository examRepository;

    @Autowired
    TeacherService teacherService;

    @Autowired
    SubjectService subjectService;

    @Override
    public List<Exam> findAll() {

        List<Exam> examList = examRepository.findAll();

        // Sort by id in ascending order
        examList.sort(Comparator.comparing(Exam::getId));

        return examList;
    }

    @Override
    public List<ExamDTOWithNames> findAllWithNames() {

        List<Exam> exams = this.examRepository.findAll();
        List<ExamDTOWithNames> examDTOs = new ArrayList<>();

        for (Exam exam : exams) {
            ExamDTOWithNames dto = new ExamDTOWithNames();
            dto.setId(exam.getId());
            dto.setDateTime(exam.getDateTime());
            dto.setName(exam.getName());
            dto.setArchived(exam.isArchived());
            dto.setSubjectName(findOneById(exam.getId()).getSubject().getName());
            dto.setTeacherName(findOneById(exam.getId()).getTeacher().getName());
            examDTOs.add(dto);
        }

        // Sort by id in ascending order
        examDTOs.sort(Comparator.comparing(ExamDTOWithNames::getId));

        return examDTOs;
    }

    @Override
    public List<ExamDTOWithNames> findAllPerTeacherWithNames(Long id){
        List<Exam> exams = this.examRepository.findByTeacherId(id);

        List<ExamDTOWithNames> examDTOs = new ArrayList<>();

        for (Exam exam : exams) {
            ExamDTOWithNames dto = new ExamDTOWithNames();
            dto.setId(exam.getId());
            dto.setDateTime(exam.getDateTime());
            dto.setName(exam.getName());
            dto.setArchived(exam.isArchived());
            dto.setSubjectName(findOneById(exam.getId()).getSubject().getName());
            dto.setTeacherName(findOneById(exam.getId()).getTeacher().getName());
            examDTOs.add(dto);
        }

        // Sort by id in ascending order
        examDTOs.sort(Comparator.comparing(ExamDTOWithNames::getId));

        return examDTOs;
    }

    @Override
    public List<Exam> findAllPerTeacher(Long id){

        List<Exam> exams = this.examRepository.findByTeacherId(id);

        // Sort by id in ascending order
        exams.sort(Comparator.comparing(Exam::getId));

        return exams;
    }

    @Override
    public Exam findOneById(Long id) {

        Optional<Exam> examOpt = examRepository.findById(id);

        if (examOpt.isPresent()) {
            return examOpt.get();
        }

        return null;
    }

    @Override
    public ExamDTOWithNames findOneByIdWithNames(Long id) {

        Optional<Exam> examOpt = examRepository.findById(id);

        ExamDTOWithNames examWithNames = new ExamDTOWithNames();

        if (examOpt.isPresent()) {

            Subject subject = subjectService.findOneById(examOpt.get().getSubject().getId());
            Teacher teacher = teacherService.findOneById(examOpt.get().getTeacher().getId());

            examWithNames.setName(examOpt.get().getName());
            examWithNames.setId(examOpt.get().getId());
            examWithNames.setArchived(examOpt.get().isArchived());
            examWithNames.setDateTime(examOpt.get().getDateTime());
            examWithNames.setSubjectName(subject.getName());
            examWithNames.setTeacherName(teacher.getName()+" "+teacher.getLastname());

            return examWithNames;
        }

        return null;
    }

    @Override
    public Exam findOneByName(String name) {

        Optional<Exam> examOpt = examRepository.findFirstByName(name);

        if (examOpt.isPresent()) {
            return examOpt.get();
        }

        return null;
    }

    @Override
    public Exam createExam(ExamDTO examDTO) {

        Exam exam = new Exam(examDTO);

        if (examDTO.getTeacherId() != 0){
            Teacher teacher = teacherService.findOneById(examDTO.getTeacherId());
            exam.setTeacher(teacher);
        }
        if (examDTO.getSubjectId() != 0){
            Subject subject = subjectService.findOneById(examDTO.getSubjectId());
            exam.setSubject(subject);
        }

        return examRepository.save(exam);
    }

    @Override
    public Exam updateExam(ExamDTO examDTO, Long id) {

        Exam exam = this.findOneById(id);

        System.out.println("exam:");
        System.out.println(examDTO.getTeacherId());
        System.out.println(examDTO.getDateTime());


        if (exam == null){
            return null;
        }

        if (examDTO.getDateTime() != null){
            exam.setDateTime(examDTO.getDateTime());
        }

        if (examDTO.getTeacherId() != null){
            exam.setTeacher(teacherService.findOneById(examDTO.getTeacherId()));
        }

        return examRepository.save(exam);
    }

    @Override
    public Exam archiveExam(Long id) {

        Exam exam = this.findOneById(id);

        if (exam == null){
            return null;
        }

        exam.setArchived(true);

        return examRepository.save(exam);
    }
}
