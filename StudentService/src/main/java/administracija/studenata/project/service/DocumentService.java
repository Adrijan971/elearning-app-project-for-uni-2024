package administracija.studenata.project.service;

import administracija.studenata.project.dto.DocumentDTO;
import administracija.studenata.project.dto.DocumentDTOWithNames;
import administracija.studenata.project.model.Document;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface DocumentService {

    List<Document> findAll();

    List<DocumentDTOWithNames> findAllWithNames();

    List<DocumentDTOWithNames> findAllPerStudentWithNames(Long id);

    Document findOneById(Long id);

    Document uploadDocument(MultipartFile file, String name, Long description) throws IOException;

    File getDocumentByName(String documentName);

    Document createDocument(DocumentDTO documentDTO);

    Document updateDocument(DocumentDTO documentDTO, Long id);

    Document archiveDocument(Long id);

}
