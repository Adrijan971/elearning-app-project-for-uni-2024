package administracija.studenata.project.service;

import administracija.studenata.project.dto.*;
import administracija.studenata.project.model.Student;
import administracija.studenata.project.model.Teacher;

import java.util.List;

public interface TeacherService {

    List<Teacher> findAll();

    Teacher findOneByName(String name);

    Teacher findOneById(Long id);

    Teacher createTeacher(TeacherDTO teacherDTO);

    Teacher updateTeacher(TeacherDTO teacherDTO, Long id);

    Teacher archiveTeacher(Long id);

    TeacherDTO login(CredentialsDTO credentialsDto);

    Teacher register(SignUpDTO signUpDTO);

    Teacher save(Teacher teacher);


}
