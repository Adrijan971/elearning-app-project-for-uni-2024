package administracija.studenata.project.service;

import administracija.studenata.project.dto.AdministratorDTO;
import administracija.studenata.project.dto.CredentialsDTO;
import administracija.studenata.project.dto.SignUpDTO;
import administracija.studenata.project.dto.StudentDTO;
import administracija.studenata.project.model.Administrator;
import administracija.studenata.project.model.Student;

import java.util.List;

public interface AdministratorService {

    List<Administrator> findAll();

    Administrator findOneByName(String name);

    Administrator findOneById(Long id);

    Administrator createAdministrator(AdministratorDTO administratorDTO);

    Administrator updateAdministrator(AdministratorDTO administratorDTO, Long id);

    Administrator archiveAdministrator(Long id);

    AdministratorDTO login(CredentialsDTO credentialsDto);

    Administrator register(SignUpDTO signUpDTO);

}
