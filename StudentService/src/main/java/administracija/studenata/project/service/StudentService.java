package administracija.studenata.project.service;

import administracija.studenata.project.dto.CredentialsDTO;
import administracija.studenata.project.dto.ExamDTOWithNames;
import administracija.studenata.project.dto.StudentDTO;
import administracija.studenata.project.dto.SignUpDTO;
import administracija.studenata.project.model.Student;

import java.util.List;

public interface StudentService {

    List<Student> findAll();

    Student findOneByName(String name);

    Student findOneById(Long id);

    Student createStudent(StudentDTO studentDTO);

    Student updateStudent(StudentDTO studentDTO, Long id);

    Student archiveStudent(Long id);

    List<ExamDTOWithNames> findAllExamsPerStudentWithNames(Long id);

    Student addMoney(Long id, double amount);

    public boolean deductMoney(Long id, double amount);

    StudentDTO login(CredentialsDTO credentialsDto);

    Student register(SignUpDTO signUpDTO);

    Student save(Student student);

    }
