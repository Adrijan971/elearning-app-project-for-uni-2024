package administracija.studenata.project.service;

import administracija.studenata.project.dto.ExamDTO;
import administracija.studenata.project.dto.ExamDTOWithNames;
import administracija.studenata.project.dto.StudentExamDTOWithNames;
import administracija.studenata.project.model.Exam;
import administracija.studenata.project.model.Subject;

import java.util.List;

public interface ExamService {

    List<Exam> findAll();
//    List<ExamDTOWithNames> findAllPerStudentWithNames(Long id);

    List<ExamDTOWithNames> findAllPerTeacherWithNames(Long id);

    List<Exam> findAllPerTeacher(Long id);

    ExamDTOWithNames findOneByIdWithNames(Long id);

    List<ExamDTOWithNames> findAllWithNames();

    Exam findOneById(Long id);

    Exam findOneByName(String name);

    Exam createExam(ExamDTO examDTO);

    Exam updateExam(ExamDTO examDTO, Long id);

    Exam archiveExam(Long id);

}
