package administracija.studenata.project.service;

import administracija.studenata.project.dto.SubjectDTO;
import administracija.studenata.project.model.Subject;

import java.util.List;

public interface SubjectService {

    List<Subject> findAll();

    List<Subject> findAllByStudentId(Long id);

    Subject findOneByName(String name);

    Subject findOneById(Long id);

    Subject createSubject(SubjectDTO subjectDTO);

    Subject save(Subject subject);

    Subject updateSubject(SubjectDTO subjectDTO, Long id);

    Subject archiveSubject(Long id);
}
