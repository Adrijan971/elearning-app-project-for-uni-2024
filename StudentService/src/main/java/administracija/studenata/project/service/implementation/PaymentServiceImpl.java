package administracija.studenata.project.service.implementation;

import administracija.studenata.project.dto.ExamDTOWithNames;
import administracija.studenata.project.dto.PaymentDTO;
import administracija.studenata.project.dto.PaymentDTOWithNames;
import administracija.studenata.project.model.*;
import administracija.studenata.project.repository.PaymentRepository;
import administracija.studenata.project.repository.SubjectRepository;
import administracija.studenata.project.service.PaymentService;
import administracija.studenata.project.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    StudentService studentService;

    @Override
    public List<Payment> findAll() {

        List<Payment> paymentList = paymentRepository.findAll();

        // Sort by id in ascending order
        paymentList.sort(Comparator.comparing(Payment::getId));

        return paymentList;
    }

    @Override
    public List<PaymentDTOWithNames> findAllWithNames() {

        List<Payment> payments = this.paymentRepository.findAll();
        List<PaymentDTOWithNames> paymentDTOs = new ArrayList<>();

        for (Payment payment : payments) {
            PaymentDTOWithNames dto = new PaymentDTOWithNames();
            dto.setId(payment.getId());
            dto.setDateTime(payment.getDateTime());
            dto.setArchived(payment.isArchived());
            dto.setAmount(payment.getAmount());
            dto.setBankAccount(payment.getBankAccount());
            dto.setStudent(payment.getStudent().getName());
            paymentDTOs.add(dto);
        }

        // Sort by id in ascending order
        paymentDTOs.sort(Comparator.comparing(PaymentDTOWithNames::getId));

        return paymentDTOs;
    }

    @Override
    public List<PaymentDTOWithNames> findAllPerStudentWithNames(Long id){
        List<Payment> payments = this.paymentRepository.findByStudentId(id);

        List<PaymentDTOWithNames> paymentDTOs = new ArrayList<>();

        for (Payment payment : payments) {
            if(!payment.isArchived()){
                PaymentDTOWithNames dto = new PaymentDTOWithNames();
                dto.setId(payment.getId());
                dto.setDateTime(payment.getDateTime());
                dto.setArchived(payment.isArchived());
                dto.setAmount(payment.getAmount());
                dto.setBankAccount(payment.getBankAccount());
                dto.setStudent(payment.getStudent().getName());
                paymentDTOs.add(dto);
            }
        }

        // Sort by id in ascending order
        paymentDTOs.sort(Comparator.comparing(PaymentDTOWithNames::getId));

        return paymentDTOs;
    }

    @Override
    public Payment findOneById(Long id) {
        Optional<Payment> paymentOpt = paymentRepository.findById(id);

        if (paymentOpt.isPresent()) {
            return paymentOpt.get();
        }

        return null;
    }

    @Override
    public Payment createPayment(PaymentDTO paymentDTO) {

        Payment payment = new Payment(paymentDTO);

        if (paymentDTO.getStudentId() != 0){
            Student student = studentService.findOneById(paymentDTO.getStudentId());
            payment.setStudent(student);

            studentService.addMoney(paymentDTO.getStudentId(),paymentDTO.getAmount());
        }

        return paymentRepository.save(payment);
    }

    @Override
    public Payment updatePayment(PaymentDTO paymentDTO, Long id) {

        Payment payment = this.findOneById(id);

        if (payment == null){
            return null;
        }

        if (paymentDTO.getAmount() != 0){
            payment.setAmount(paymentDTO.getAmount());
        }

        if (paymentDTO.getDateTime() != null){
            payment.setDateTime(paymentDTO.getDateTime());
        }

        return paymentRepository.save(payment);
    }

    @Override
    public Payment archivePayment(Long id) {

        Payment payment = this.findOneById(id);

        if (payment == null){
            return null;
        }

        payment.setArchived(true);

        return paymentRepository.save(payment);
    }
}
