package administracija.studenata.project.repository;

import administracija.studenata.project.model.Administrator;
import administracija.studenata.project.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long> {

    Optional<Subject> findFirstByName(String name);

    @Query("SELECT subj FROM Subject subj JOIN subj.students stud WHERE stud.id = :studentId")
    List<Subject> findSubjectsByStudentId(@Param("studentId") Long studentId);

}
