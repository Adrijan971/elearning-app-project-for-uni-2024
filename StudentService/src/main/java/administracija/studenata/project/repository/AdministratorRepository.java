package administracija.studenata.project.repository;

import administracija.studenata.project.model.Administrator;
import administracija.studenata.project.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Long> {

    Optional<Administrator> findFirstByName(String name);

    Optional<Administrator> findFirstByUsername(String Username);

}
