package administracija.studenata.project.repository;

import administracija.studenata.project.model.Administrator;
import administracija.studenata.project.model.Exam;
import administracija.studenata.project.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {

    List<Payment> findByStudentId(Long studentId);

}
