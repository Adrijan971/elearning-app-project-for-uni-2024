package administracija.studenata.project.repository;

import administracija.studenata.project.model.Administrator;
import administracija.studenata.project.model.Exam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ExamRepository extends JpaRepository<Exam, Long> {

    Optional<Exam> findFirstByName(String name);

    List<Exam> findByTeacherId(Long teacherId);

//    List<Exam> findByStudentId(Long studentId);

}
