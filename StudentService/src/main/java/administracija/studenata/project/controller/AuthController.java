package administracija.studenata.project.controller;

import administracija.studenata.project.config.UserAuthProvider;
import administracija.studenata.project.dto.*;
import administracija.studenata.project.enums.Role;
import administracija.studenata.project.service.StudentService;
import administracija.studenata.project.service.AdministratorService;
import administracija.studenata.project.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

    @Autowired
    StudentService studentService;

    @Autowired
    AdministratorService administratorService;

    @Autowired
    TeacherService teacherService;

    @Autowired
    UserAuthProvider userAuthProvider;

    @PostMapping("/login")
    public ResponseEntity<UserDTO> login(@RequestBody CredentialsDTO credentialsDto){

        if (credentialsDto.userType().equals(Role.ADMINISTRATOR)){
            AdministratorDTO administratorDTO = administratorService.login(credentialsDto);
            administratorDTO.setToken(userAuthProvider.createToken(administratorDTO));

            return ResponseEntity.ok(administratorDTO);

        } else if (credentialsDto.userType().equals(Role.STUDENT)) {
            StudentDTO studentDto = studentService.login(credentialsDto);
            studentDto.setToken(userAuthProvider.createToken(studentDto));

            return ResponseEntity.ok(studentDto);

        } else if (credentialsDto.userType().equals(Role.TEACHER)) {
            TeacherDTO teacherDto = teacherService.login(credentialsDto);
            teacherDto.setToken(userAuthProvider.createToken(teacherDto));

            return ResponseEntity.ok(teacherDto);
        }

        return null;
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody SignUpDTO signUpDto){

        if (signUpDto.userType().equals(Role.ADMINISTRATOR)){
            administratorService.register(signUpDto);
            return new ResponseEntity<>(HttpStatus.OK);
        } else if (signUpDto.userType().equals(Role.STUDENT)) {
            studentService.register(signUpDto);
            return new ResponseEntity<>(HttpStatus.OK);
        } else if (signUpDto.userType().equals(Role.TEACHER)) {
            teacherService.register(signUpDto);
            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
