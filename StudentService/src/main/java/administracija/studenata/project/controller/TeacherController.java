package administracija.studenata.project.controller;

import administracija.studenata.project.dto.SubjectDTO;
import administracija.studenata.project.dto.TeacherDTO;
import administracija.studenata.project.model.Student;
import administracija.studenata.project.model.Subject;
import administracija.studenata.project.model.Teacher;
import administracija.studenata.project.service.SubjectService;
import administracija.studenata.project.service.TeacherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
@RequestMapping("api/teachers")
public class TeacherController {

    @Autowired
    TeacherService teacherService;

    @Autowired
    SubjectService subjectService;

    @GetMapping
    public ResponseEntity<List<TeacherDTO>> findAll(){
        List<Teacher> teachers = teacherService.findAll();

        List<TeacherDTO> teacherDTOS = new ArrayList<>();

        for (Teacher teacher : teachers){
            teacherDTOS.add(new TeacherDTO(teacher));
        }

        return new ResponseEntity<>(teacherDTOS, HttpStatus.OK);
    };

    @PostMapping("/create")
    public ResponseEntity<TeacherDTO> create(@RequestBody TeacherDTO newTeacherDto){

        Teacher createdTeacher = teacherService.createTeacher(newTeacherDto);

        if(createdTeacher == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        TeacherDTO teacherDTO = new TeacherDTO(createdTeacher);

        return new ResponseEntity<>(teacherDTO, HttpStatus.CREATED);
    }

    @GetMapping("/subjectsByTeacherId/{id}")
    public ResponseEntity<List<SubjectDTO>> findAllByTeacherId(@PathVariable("id") Long id){

        Teacher teacher = teacherService.findOneById(id);

        Set<Subject> subjects = teacher.getSubjects();

        List<SubjectDTO> subjectDTOS = new ArrayList<>();

        for (Subject subject : subjects){

            if(!subject.isArchived()){
                subjectDTOS.add(new SubjectDTO(subject));
            }
        }

        return new ResponseEntity<>(subjectDTOS, HttpStatus.OK);
    };

    @PostMapping("/linkSubjectToTeacher/{teacherId}/{subjectId}")
    public ResponseEntity<Map<String, String>> linkSubjectToTeacher(@PathVariable Long teacherId, @PathVariable Long subjectId) {

        Logger logger = LoggerFactory.getLogger(StudentController.class);

        // Log the IDs
        logger.info("Linking Teacher to Student");
        logger.info("Student ID: " + teacherId);
        logger.info("Subject ID: " + subjectId);


        Teacher teacher = teacherService.findOneById(teacherId);
        Subject subject = subjectService.findOneById(subjectId);

        if (teacher == null || subject == null) {
            return new ResponseEntity<>(Collections.singletonMap("message", "Teacher or Subject not found"), HttpStatus.NOT_FOUND);
        }

        // Link the subject to the student
        teacher.getSubjects().add(subject);

        // Link the student to the subject (other side of the relation)
        subject.getTeachers().add(teacher);

        // Save both entities to persist the relationship on both sides
        teacherService.save(teacher);
        subjectService.save(subject);

        return new ResponseEntity<>(Collections.singletonMap("message", "Subject linked to teacher successfully"), HttpStatus.OK);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<TeacherDTO> findOneById(@PathVariable("id") Long id){

        Teacher teacher = teacherService.findOneById(id);

        if (teacher == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        TeacherDTO teacherDTO = new TeacherDTO(teacher);

        return new ResponseEntity<>(teacherDTO, HttpStatus.OK);
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<TeacherDTO> findOneByName(@PathVariable("name") String name){

        Teacher teacher = teacherService.findOneByName(name);

        if (teacher == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        TeacherDTO teacherDTO = new TeacherDTO(teacher);

        return new ResponseEntity<>(teacherDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/archive/{id}")
    public ResponseEntity<TeacherDTO> archiveTeacher(@PathVariable Long id) {

        Teacher teacher = teacherService.archiveTeacher(id);

        if (teacher == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new TeacherDTO(teacher), HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", consumes = "application/json")
    public ResponseEntity<TeacherDTO> updateTeacher(@RequestBody TeacherDTO teacherDTO, @PathVariable("id") Long id) {

        Teacher teacher = teacherService.updateTeacher(teacherDTO,id);

        if (teacher == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new TeacherDTO(teacher), HttpStatus.OK);
    }

}
