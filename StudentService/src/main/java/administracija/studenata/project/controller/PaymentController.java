package administracija.studenata.project.controller;

import administracija.studenata.project.dto.ExamDTOWithNames;
import administracija.studenata.project.dto.PaymentDTO;
import administracija.studenata.project.dto.PaymentDTOWithNames;
import administracija.studenata.project.model.Payment;
import administracija.studenata.project.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("api/payments")
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @GetMapping
    public ResponseEntity<List<PaymentDTO>> findAll(){
        List<Payment> payments = paymentService.findAll();

        List<PaymentDTO> paymentDTOS = new ArrayList<>();

        for (Payment payment : payments){
            paymentDTOS.add(new PaymentDTO(payment));
        }

        return new ResponseEntity<>(paymentDTOS, HttpStatus.OK);
    };

    // switch studentId's with actual names
    @GetMapping("/listWithNames")
    public ResponseEntity<List<PaymentDTOWithNames>> findAllWithNames(){
        List<PaymentDTOWithNames> paymentsDTO = paymentService.findAllWithNames();

        return new ResponseEntity<>(paymentsDTO, HttpStatus.OK);
    };

    @PostMapping("/create")
    public ResponseEntity<PaymentDTO> create(@RequestBody PaymentDTO newPaymentDto){

        Payment createdPayment = paymentService.createPayment(newPaymentDto);

        if(createdPayment == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        PaymentDTO paymentDTO = new PaymentDTO(createdPayment);

        return new ResponseEntity<>(paymentDTO, HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<PaymentDTO> findOneById(@PathVariable("id") Long id){

        Payment payment = paymentService.findOneById(id);

        if (payment == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        PaymentDTO paymentDTO = new PaymentDTO(payment);

        return new ResponseEntity<>(paymentDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/archive/{id}")
    public ResponseEntity<PaymentDTO> archivePayment(@PathVariable Long id) {

        Payment payment = paymentService.archivePayment(id);

        if (payment == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new PaymentDTO(payment), HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", consumes = "application/json")
    public ResponseEntity<PaymentDTO> updatePayment(@RequestBody PaymentDTO paymentDTO, @PathVariable("id") Long id) {

        Payment payment = paymentService.updatePayment(paymentDTO,id);

        if (payment == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new PaymentDTO(payment), HttpStatus.OK);
    }

}
