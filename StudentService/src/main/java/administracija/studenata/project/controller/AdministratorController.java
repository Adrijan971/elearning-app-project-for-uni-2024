package administracija.studenata.project.controller;

import administracija.studenata.project.dto.AdministratorDTO;
import administracija.studenata.project.dto.StudentDTO;
import administracija.studenata.project.model.Administrator;
import administracija.studenata.project.model.Student;
import administracija.studenata.project.service.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/administrators")
public class AdministratorController {

    @Autowired
    AdministratorService administratorService;

    @GetMapping
    public ResponseEntity<List<AdministratorDTO>> findAll(){
        List<Administrator> administrators = administratorService.findAll();

        List<AdministratorDTO> administratorDTOS = new ArrayList<>();

        for (Administrator administrator : administrators){
            administratorDTOS.add(new AdministratorDTO(administrator));
        }

        return new ResponseEntity<>(administratorDTOS, HttpStatus.OK);
    };

    @PostMapping("/create")
    public ResponseEntity<AdministratorDTO> create(@RequestBody AdministratorDTO newAdministratorDto){

        Administrator createdadministrator = administratorService.createAdministrator(newAdministratorDto);

        if(createdadministrator == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        AdministratorDTO administratorDTO = new AdministratorDTO(createdadministrator);

        return new ResponseEntity<>(administratorDTO, HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<AdministratorDTO> findOneById(@PathVariable("id") Long id){

        Administrator administrator = administratorService.findOneById(id);

        if (administrator == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        AdministratorDTO administratorDTO = new AdministratorDTO(administrator);

        return new ResponseEntity<>(administratorDTO, HttpStatus.OK);
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<AdministratorDTO> findOneByName(@PathVariable("name") String name){

        Administrator administrator = administratorService.findOneByName(name);

        if (administrator == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        AdministratorDTO administratorDTO = new AdministratorDTO(administrator);

        return new ResponseEntity<>(administratorDTO, HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", consumes = "application/json")
    public ResponseEntity<AdministratorDTO> updateAdministrator(@RequestBody AdministratorDTO administratorDTO, @PathVariable("id") Long id) {

        Administrator administrator = administratorService.updateAdministrator(administratorDTO,id);

        if (administrator == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new AdministratorDTO(administrator), HttpStatus.OK);
    }

    @DeleteMapping(value = "/archive/{id}")
    public ResponseEntity<AdministratorDTO> archiveAdministrator(@PathVariable Long id) {

        Administrator administrator = administratorService.archiveAdministrator(id);

        if (administrator == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new AdministratorDTO(administrator), HttpStatus.OK);
    }

}
