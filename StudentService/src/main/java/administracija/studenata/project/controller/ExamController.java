package administracija.studenata.project.controller;

import administracija.studenata.project.dto.ExamDTO;
import administracija.studenata.project.dto.ExamDTOWithNames;
import administracija.studenata.project.dto.SubjectDTO;
import administracija.studenata.project.model.Exam;
import administracija.studenata.project.model.Subject;
import administracija.studenata.project.service.ExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("api/exams")
public class ExamController {


    @Autowired
    ExamService examService;

    @GetMapping("/list")
    public ResponseEntity<List<ExamDTO>> findAll(){
        List<Exam> exams = examService.findAll();

        List<ExamDTO> examDTOS = new ArrayList<>();

        for (Exam exam : exams){
            examDTOS.add(new ExamDTO(exam));
        }

        return new ResponseEntity<>(examDTOS, HttpStatus.OK);
    };

    @GetMapping("/listPerTeacherWithNames/{id}")
    public ResponseEntity<List<ExamDTOWithNames>> findAllPerTeacherWithNames(@PathVariable("id") Long id){
        List<ExamDTOWithNames> examsDTO = examService.findAllPerTeacherWithNames(id);

        return new ResponseEntity<>(examsDTO, HttpStatus.OK);
    };

    @GetMapping("/listWithNames")
    public ResponseEntity<List<ExamDTOWithNames>> findAllWithNames(){
        List<ExamDTOWithNames> examsDTO = examService.findAllWithNames();

        return new ResponseEntity<>(examsDTO, HttpStatus.OK);
    };

    @PostMapping("/create")
    public ResponseEntity<ExamDTO> create(@RequestBody ExamDTO newExamDto){

        Exam createdExam = examService.createExam(newExamDto);

        if(createdExam == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        ExamDTO examDTO = new ExamDTO(createdExam);

        return new ResponseEntity<>(examDTO, HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<ExamDTO> findOneById(@PathVariable("id") Long id){

        Exam exam = examService.findOneById(id);

        if (exam == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        ExamDTO examDTO = new ExamDTO(exam);

        return new ResponseEntity<>(examDTO, HttpStatus.OK);
    }

    // with names
    @GetMapping("/id-with-names/{id}")
    public ResponseEntity<ExamDTOWithNames> findOneByIdWithNames(@PathVariable("id") Long id){

        ExamDTOWithNames examDTOWithNames = examService.findOneByIdWithNames(id);

        if (examDTOWithNames == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(examDTOWithNames, HttpStatus.OK);
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<ExamDTO> findOneByName(@PathVariable("name") String name){

        Exam exam = examService.findOneByName(name);

        if (exam == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        ExamDTO examDTO = new ExamDTO(exam);

        return new ResponseEntity<>(examDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/archive/{id}")
    public ResponseEntity<ExamDTO> archiveExam(@PathVariable Long id) {

        Exam exam = examService.archiveExam(id);

        if (exam == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new ExamDTO(exam), HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", consumes = "application/json")
    public ResponseEntity<ExamDTO> updateExam(@RequestBody ExamDTO examDTO, @PathVariable("id") Long id) {

        Exam exam = examService.updateExam(examDTO,id);

        if (exam == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new ExamDTO(exam), HttpStatus.OK);
    }
}
