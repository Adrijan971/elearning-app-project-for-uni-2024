package administracija.studenata.project.controller;

import administracija.studenata.project.dto.ExamDTOWithNames;
import administracija.studenata.project.dto.StudentExamDTO;
import administracija.studenata.project.dto.StudentExamDTOWithNames;
import administracija.studenata.project.model.StudentExam;
import administracija.studenata.project.service.StudentExamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/applications")
public class StudentExamController {

    @Autowired
    StudentExamService studentExamService;

    @GetMapping("list")
    public ResponseEntity<List<StudentExamDTO>> findAll(){
        List<StudentExam> studentExams = studentExamService.findAll();

        List<StudentExamDTO> studentExamDTOs = new ArrayList<>();

        for (StudentExam studentExam : studentExams){
            studentExamDTOs.add(new StudentExamDTO(studentExam));
        }

        return new ResponseEntity<>(studentExamDTOs, HttpStatus.OK);
    };

    @GetMapping("/listWithNames")
    public ResponseEntity<List<StudentExamDTOWithNames>> findAllWithNames(){
        List<StudentExamDTOWithNames> applciationsWithNames = studentExamService.findAllWithNames();

        return new ResponseEntity<>(applciationsWithNames, HttpStatus.OK);
    };

    @GetMapping("/listPerStudentWithNames/{id}")
    public ResponseEntity<List<StudentExamDTOWithNames>> findAllPerStudentWithNames(@PathVariable("id") Long id){
        List<StudentExamDTOWithNames> applicationsDTO = studentExamService.findAllPerStudentWithNames(id);

        return new ResponseEntity<>(applicationsDTO, HttpStatus.OK);
    };

    @GetMapping("/listPerTeacherWithNames/{id}")
    public ResponseEntity<List<StudentExamDTOWithNames>> findAllPerTeacherWithNames(@PathVariable("id") Long id){
        List<StudentExamDTOWithNames> applicationsDTO = studentExamService.findAllPerTeacherWithNames(id);

        return new ResponseEntity<>(applicationsDTO, HttpStatus.OK);
    };

    @GetMapping("/listPerExamWithNames/{id}")
    public ResponseEntity<List<StudentExamDTOWithNames>> findAllPerExamWithNames(@PathVariable("id") Long id){
        List<StudentExamDTOWithNames> applicationsDTO = studentExamService.findAllPerExamWithNames(id);

        return new ResponseEntity<>(applicationsDTO, HttpStatus.OK);
    };

    @PostMapping("/create")
    public ResponseEntity<StudentExamDTO> create(@RequestBody StudentExamDTO newStudentExamDto){

        StudentExam createdStudentExam = studentExamService.createStudentExam(newStudentExamDto);

        if(createdStudentExam == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        StudentExamDTO studentExamDTO = new StudentExamDTO(createdStudentExam);

        return new ResponseEntity<>(studentExamDTO, HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<StudentExamDTO> findOneById(@PathVariable("id") Long id){

        StudentExam studentExam = studentExamService.findOneById(id);

        if (studentExam == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        StudentExamDTO studentExamDTO = new StudentExamDTO(studentExam);

        return new ResponseEntity<>(studentExamDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/archive/{id}")
    public ResponseEntity<StudentExamDTO> archiveStudentExam(@PathVariable Long id) {

        StudentExam studentExam = studentExamService.archiveStudentExam(id);

        if (studentExam == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new StudentExamDTO(studentExam), HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", consumes = "application/json")
    public ResponseEntity<StudentExamDTO> updateStudentExam(@RequestBody StudentExamDTO studentExamDTO, @PathVariable("id") Long id) {

        StudentExam studentExam = studentExamService.updateStudentExam(studentExamDTO,id);

        if (studentExam == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new StudentExamDTO(studentExam), HttpStatus.OK);
    }

    @PutMapping(value = "/pass/{id}/{grade}", consumes = "application/json")
    public ResponseEntity<StudentExamDTO> passStudentExam(@PathVariable("id") Long id, @PathVariable("grade") int grade) {

        StudentExam studentExam = studentExamService.passStudentExam(id, grade);

        if (studentExam == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new StudentExamDTO(studentExam), HttpStatus.OK);
    }



}
