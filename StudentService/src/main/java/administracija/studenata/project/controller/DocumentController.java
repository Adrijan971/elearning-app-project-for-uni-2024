package administracija.studenata.project.controller;

import administracija.studenata.project.dto.DocumentDTO;
import administracija.studenata.project.dto.DocumentDTOWithNames;
import administracija.studenata.project.dto.ExamDTOWithNames;
import administracija.studenata.project.dto.SubjectDTO;
import administracija.studenata.project.model.Document;
import administracija.studenata.project.model.Subject;
import administracija.studenata.project.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.HttpHeaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("api/documents")
public class DocumentController {

    @Autowired
    DocumentService documentService;

    @GetMapping
    public ResponseEntity<List<DocumentDTO>> findAll(){
        List<Document> documents = documentService.findAll();

        List<DocumentDTO> documentDTOS = new ArrayList<>();

        for (Document document : documents){
            documentDTOS.add(new DocumentDTO(document));
        }

        return new ResponseEntity<>(documentDTOS, HttpStatus.OK);
    };

    @GetMapping("/listWithNames")
    public ResponseEntity<List<DocumentDTOWithNames>> findAllWithNames(){
        List<DocumentDTOWithNames> documentDTOs = documentService.findAllWithNames();

        return new ResponseEntity<>(documentDTOs, HttpStatus.OK);
    };

    @GetMapping("/listPerStudentWithNames/{id}")
    public ResponseEntity<List<DocumentDTOWithNames>> findAllPerStudentWithNames(@PathVariable("id") Long id){
        List<DocumentDTOWithNames> documentsDTO = documentService.findAllPerStudentWithNames(id);

        return new ResponseEntity<>(documentsDTO, HttpStatus.OK);
    };

    @PostMapping("/upload")
    public ResponseEntity<Document> uploadDocument(@RequestParam("file") MultipartFile file,
                                                   @RequestParam("name") String name,
                                                    @RequestParam("student_id") Long student_id) {
        try {
            // Call the service to upload the document
            Document document = documentService.uploadDocument(file, name, student_id);

            // Return the uploaded document as the response
            return new ResponseEntity<>(document, HttpStatus.CREATED);
        } catch (IOException e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // New endpoint to download a document
    @GetMapping("/download/{documentName}")
    public ResponseEntity<InputStreamResource> downloadDocument(@PathVariable String documentName) {
        try {
            // Fetch the document from the service layer
            File file = documentService.getDocumentByName(documentName);

            if (file == null || !file.exists()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build(); // Return 404 if the file does not exist
            }

            // Create an InputStream from the file
            InputStream inputStream = new FileInputStream(file);

            // Prepare headers for downloading the file
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getName());
            headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_VALUE);
            headers.add(HttpHeaders.CONTENT_LENGTH, String.valueOf(file.length()));

            // Return the file as a downloadable response
            return new ResponseEntity<>(new InputStreamResource(inputStream), headers, HttpStatus.OK);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build(); // Return 500 if an error occurs
        }
    }


    @PostMapping("/create")
    public ResponseEntity<DocumentDTO> create(@RequestBody DocumentDTO newDocumentDto){

        Document createdDocument = documentService.createDocument(newDocumentDto);

        if(createdDocument == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        DocumentDTO documentDTO = new DocumentDTO(createdDocument);

        return new ResponseEntity<>(documentDTO, HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<DocumentDTO> findOneById(@PathVariable("id") Long id){

        Document document = documentService.findOneById(id);

        if (document == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        DocumentDTO documentDTO = new DocumentDTO(document);

        return new ResponseEntity<>(documentDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/archive/{id}")
    public ResponseEntity<DocumentDTO> archiveDocument(@PathVariable Long id) {

        Document document = documentService.archiveDocument(id);

        if (document == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new DocumentDTO(document), HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", consumes = "application/json")
    public ResponseEntity<DocumentDTO> updateDocument(@RequestBody DocumentDTO documentDTO, @PathVariable("id") Long id) {

        Document document = documentService.updateDocument(documentDTO,id);

        if (document == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new DocumentDTO(document), HttpStatus.OK);
    }
}
