package administracija.studenata.project.controller;

import administracija.studenata.project.dto.*;
import administracija.studenata.project.model.Student;
import administracija.studenata.project.model.Subject;
import administracija.studenata.project.model.Teacher;
import administracija.studenata.project.service.PaymentService;
import administracija.studenata.project.service.StudentService;
import administracija.studenata.project.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

@RestController
@RequestMapping("api/students")
public class StudentController {

    @Autowired
    StudentService studentService;

    @Autowired
    SubjectService subjectService;

    @Autowired
    PaymentService paymentService;

    @GetMapping
    public ResponseEntity<List<StudentDTO>> findAll(){
        List<Student> students = studentService.findAll();

        Logger logger = LoggerFactory.getLogger(StudentController.class);

        // Log the IDs
        logger.info("TEST");

        List<StudentDTO> studentDTOS = new ArrayList<>();

        for (Student student : students){
            studentDTOS.add(new StudentDTO(student));
        }

        return new ResponseEntity<>(studentDTOS, HttpStatus.OK);
    };

    @GetMapping("/subjectsByStudentId/{id}")
    public ResponseEntity<List<SubjectDTO>> findAllByStudentId(@PathVariable("id") Long id){

        Student student = studentService.findOneById(id);

        Set<Subject> subjects = student.getSubjects();

        List<SubjectDTO> subjectDTOS = new ArrayList<>();

        for (Subject subject : subjects){
            if(!subject.isArchived()){
                subjectDTOS.add(new SubjectDTO(subject));
            }
        }

        return new ResponseEntity<>(subjectDTOS, HttpStatus.OK);
    };

    @GetMapping("/paymentsByStudentId/{id}")
    public ResponseEntity<List<PaymentDTOWithNames>> findAllPaymentsByStudentId(@PathVariable("id") Long id){

        List<PaymentDTOWithNames> paymentDTO = paymentService.findAllPerStudentWithNames(id);

        return new ResponseEntity<>(paymentDTO, HttpStatus.OK);
    };

    @GetMapping("/examsByStudentId/{id}")
    public ResponseEntity<List<ExamDTOWithNames>> findAllExamsByStudentId(@PathVariable("id") Long id){

        List<ExamDTOWithNames> examsDTO = studentService.findAllExamsPerStudentWithNames(id);

        return new ResponseEntity<>(examsDTO, HttpStatus.OK);
    };

    @PostMapping("/linkSubjectToStudent/{studentId}/{subjectId}")
    public ResponseEntity<Map<String, String>> linkSubjectToStudent(@PathVariable Long studentId, @PathVariable Long subjectId) {
        // Find the student and subject entities


        Logger logger = LoggerFactory.getLogger(StudentController.class);

        // Log the IDs
        logger.info("Linking Subject to Student");
        logger.info("Student ID: " + studentId);
        logger.info("Subject ID: " + subjectId);


        Student student = studentService.findOneById(studentId);
        Subject subject = subjectService.findOneById(subjectId);

        if (student == null || subject == null) {
            return new ResponseEntity<>(Collections.singletonMap("message", "Student or Subject not found"), HttpStatus.NOT_FOUND);
        }

        // Link the subject to the student
        student.getSubjects().add(subject);

        // Link the student to the subject (other side of the relation)
        subject.getStudents().add(student);

        // Save both entities to persist the relationship on both sides
        studentService.save(student);
        subjectService.save(subject);

        return new ResponseEntity<>(Collections.singletonMap("message", "Subject linked to student successfully"), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<StudentDTO> create(@RequestBody StudentDTO newStudentDto){

        Student createdStudent = studentService.createStudent(newStudentDto);

        if(createdStudent == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        StudentDTO studentDTO = new StudentDTO(createdStudent);

        return new ResponseEntity<>(studentDTO, HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<StudentDTO> findOneById(@PathVariable("id") Long id){

        Student student = studentService.findOneById(id);

        if (student == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        StudentDTO studentDTO = new StudentDTO(student);

        return new ResponseEntity<>(studentDTO, HttpStatus.OK);
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<StudentDTO> findOneByName(@PathVariable("name") String name){

        Student student = studentService.findOneByName(name);

        if (student == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        StudentDTO studentDTO = new StudentDTO(student);

        return new ResponseEntity<>(studentDTO, HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", consumes = "application/json")
    public ResponseEntity<StudentDTO> updateStudent(@RequestBody StudentDTO studentDTO, @PathVariable("id") Long id) {

        Student student = studentService.updateStudent(studentDTO,id);

        if (student == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new StudentDTO(student), HttpStatus.OK);
    }

    @DeleteMapping(value = "/archive/{id}")
    public ResponseEntity<StudentDTO> archiveStudent(@PathVariable Long id) {

        Student student = studentService.archiveStudent(id);

        if (student == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new StudentDTO(student), HttpStatus.OK);
    }

}
