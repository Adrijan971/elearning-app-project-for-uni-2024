package administracija.studenata.project.controller;

import administracija.studenata.project.dto.SubjectDTO;
import administracija.studenata.project.model.Subject;
import administracija.studenata.project.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("api/subjects")
public class SubjectController {

    @Autowired
    SubjectService subjectService;

    @GetMapping
    public ResponseEntity<List<SubjectDTO>> findAll(){
        List<Subject> subjects = subjectService.findAll();

        List<SubjectDTO> subjectDTOS = new ArrayList<>();

        for (Subject subject : subjects){
            subjectDTOS.add(new SubjectDTO(subject));
        }

        return new ResponseEntity<>(subjectDTOS, HttpStatus.OK);
    };

    @PostMapping("/create")
    public ResponseEntity<SubjectDTO> create(@RequestBody SubjectDTO newSubjectDto){

        Subject createdSubject = subjectService.createSubject(newSubjectDto);

        if(createdSubject == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_ACCEPTABLE);
        }

        SubjectDTO subjectDTO = new SubjectDTO(createdSubject);

        return new ResponseEntity<>(subjectDTO, HttpStatus.CREATED);
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<SubjectDTO> findOneById(@PathVariable("id") Long id){

        Subject subject = subjectService.findOneById(id);

        if (subject == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        SubjectDTO subjectDTO = new SubjectDTO(subject);

        return new ResponseEntity<>(subjectDTO, HttpStatus.OK);
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<SubjectDTO> findOneByName(@PathVariable("name") String name){

        Subject subject = subjectService.findOneByName(name);

        if (subject == null){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        SubjectDTO subjectDTO = new SubjectDTO(subject);

        return new ResponseEntity<>(subjectDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/archive/{id}")
    public ResponseEntity<SubjectDTO> archiveSubject(@PathVariable Long id) {

        Subject subject = subjectService.archiveSubject(id);

        if (subject == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new SubjectDTO(subject), HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}", consumes = "application/json")
    public ResponseEntity<SubjectDTO> updateSubject(@RequestBody SubjectDTO subjectDTO, @PathVariable("id") Long id) {

        Subject subject = subjectService.updateSubject(subjectDTO,id);

        if (subject == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new SubjectDTO(subject), HttpStatus.OK);
    }

}
