package administracija.studenata.project.dto;

import administracija.studenata.project.enums.Role;
import administracija.studenata.project.model.*;

import java.util.Set;

public class StudentDTO extends UserDTO{

    private String address;
    private int mobilePhone;
    private Role role;
    private boolean archived;
    private String bankAccount;
    private double accountBalance;
    private int indexNumber;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(int mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public int getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(int indexNumber) {
        this.indexNumber = indexNumber;
    }


    public StudentDTO() {
        super(); // Poziva prazan konstruktor osnovne klase UserDTO
    }

    public StudentDTO(Student student) {

        super(student.getId(), student.getUsername(), student.getName(), student.getLastname()); // Poziv konstruktora osnovne klase

        this.address = student.getAddress();
        this.mobilePhone = student.getMobilePhone();
        this.role = student.getRole();
        this.archived = student.isArchived();
        this.bankAccount = student.getBankAccount();
        this.accountBalance = student.getAccountBalance();
        this.indexNumber = student.getIndexNumber();
    }

}
