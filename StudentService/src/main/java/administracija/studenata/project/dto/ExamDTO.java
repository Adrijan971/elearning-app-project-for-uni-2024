package administracija.studenata.project.dto;

import administracija.studenata.project.model.Exam;
import administracija.studenata.project.model.StudentExam;
import administracija.studenata.project.model.Subject;
import administracija.studenata.project.model.Teacher;

import java.time.LocalDateTime;
import java.util.Set;

public class ExamDTO {

    private Long id;
    private LocalDateTime dateTime;
    private boolean archived;
    private String name;
    private Long subjectId;
    private Long teacherId;
    private Set<StudentExam> applications;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Set<StudentExam> getApplications() {
        return applications;
    }

    public void setApplications(Set<StudentExam> applications) {
        this.applications = applications;
    }

    public ExamDTO() {
    }

    public ExamDTO(Exam exam) {

        this.id = exam.getId();
        this.archived = exam.isArchived();
        this.dateTime = exam.getDateTime();
        this.name = exam.getName();
        this.subjectId = exam.getSubject().getId();
        this.teacherId = exam.getTeacher().getId();
        this.applications = exam.getApplications();
    }

}
