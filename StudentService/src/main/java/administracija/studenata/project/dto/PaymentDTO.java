package administracija.studenata.project.dto;

import administracija.studenata.project.model.Payment;
import administracija.studenata.project.model.Student;

import java.time.LocalDateTime;

public class PaymentDTO {

    private Long id;
    private double amount;
    private String bankAccount;
    private LocalDateTime dateTime;
    private boolean archived;
    private Long studentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public PaymentDTO() {
    }

    public PaymentDTO(Payment payment) {

        this.id = payment.getId();
        this.archived = payment.isArchived();
        this.amount = payment.getAmount();
        this.dateTime = payment.getDateTime();
        this.bankAccount = payment.getBankAccount();
        this.studentId = payment.getStudent().getId();
    }

}
