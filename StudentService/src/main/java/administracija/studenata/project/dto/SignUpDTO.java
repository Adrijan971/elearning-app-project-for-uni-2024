package administracija.studenata.project.dto;

import administracija.studenata.project.enums.Role;
import administracija.studenata.project.enums.TeacherRole;

public record SignUpDTO(String firstName, String lastName, String username, char[] password, Role userType, TeacherRole teacherType){
}
