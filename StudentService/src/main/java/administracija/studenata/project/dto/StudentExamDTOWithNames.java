package administracija.studenata.project.dto;

import administracija.studenata.project.model.StudentExam;

import java.time.LocalDateTime;

public class StudentExamDTOWithNames {


    private Long id;
    private LocalDateTime applicationDate;
    private boolean passed;
    private int grade;
    private boolean archived;
    private String student;
    private String exam;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(LocalDateTime applicationDate) {
        this.applicationDate = applicationDate;
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public int getGrade() { return grade; }

    public void setGrade(int grade) { this.grade = grade; }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }

    public StudentExamDTOWithNames() {
    }

    public StudentExamDTOWithNames(StudentExam studentExam) {

        this.id = studentExam.getId();
        this.applicationDate = studentExam.getApplicationDate();
        this.passed = studentExam.isPassed();
        this.grade = studentExam.getGrade();
        this.archived = studentExam.isArchived();
//        this.examId = studentExam.getExam().getId();
//        this.studentId = studentExam.getStudent().getId();
    }


}
