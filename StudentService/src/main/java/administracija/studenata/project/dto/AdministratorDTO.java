package administracija.studenata.project.dto;

import administracija.studenata.project.enums.Role;
import administracija.studenata.project.model.Administrator;

public class AdministratorDTO extends UserDTO {

    private String address;
    private int mobilePhone;
    private Role role;
    private boolean archived;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(int mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }


    public AdministratorDTO() {
        super(); // Poziva prazan konstruktor osnovne klase UserDTO
    }

    public AdministratorDTO(Administrator administrator) {

        super(administrator.getId(), administrator.getUsername(), administrator.getName(), administrator.getLastname()); // Poziv konstruktora osnovne klase

        this.address = administrator.getAddress();
        this.mobilePhone = administrator.getMobilePhone();
        this.role = administrator.getRole();
        this.archived = administrator.isArchived();
    }

}
