package administracija.studenata.project.dto;

import administracija.studenata.project.model.Exam;
import administracija.studenata.project.model.Student;
import administracija.studenata.project.model.StudentExam;
import jakarta.persistence.*;

import java.time.LocalDateTime;

public class StudentExamDTO {


    private Long id;
    private LocalDateTime applicationDate;
    private boolean passed;
    private int grade;
    private boolean archived;
    private Long studentId;
    private Long examId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(LocalDateTime applicationDate) {
        this.applicationDate = applicationDate;
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public int getGrade() {return grade;}

    public void setGrade(int grade) {this.grade = grade;}

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public StudentExamDTO() {
    }

    public StudentExamDTO(StudentExam studentExam) {

        this.id = studentExam.getId();
        this.applicationDate = studentExam.getApplicationDate();
        this.passed = studentExam.isPassed();
        this.grade = studentExam.getGrade();
        this.archived = studentExam.isArchived();
        this.examId = studentExam.getExam().getId();
        this.studentId = studentExam.getStudent().getId();
    }


}
