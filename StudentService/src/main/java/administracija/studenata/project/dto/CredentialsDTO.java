package administracija.studenata.project.dto;

import administracija.studenata.project.enums.Role;
public record CredentialsDTO(String username, char[] password, Role userType) {
}
