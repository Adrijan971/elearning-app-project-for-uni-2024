package administracija.studenata.project.dto;

import administracija.studenata.project.model.Exam;
import administracija.studenata.project.model.StudentExam;

import java.time.LocalDateTime;
import java.util.Set;

public class ExamDTOWithNames {

        private Long id;
        private LocalDateTime dateTime;
        private boolean archived;
        private String name;
        private String subjectName;
        private String teacherName;
        private Set<StudentExam> applications;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public LocalDateTime getDateTime() {
            return dateTime;
        }

        public void setDateTime(LocalDateTime dateTime) {
            this.dateTime = dateTime;
        }

        public boolean isArchived() {
            return archived;
        }

        public void setArchived(boolean archived) {
            this.archived = archived;
        }

        public String getName() { return name; }

        public void setName(String name) { this.name = name; }

        public String getSubjectName() {
            return subjectName;
        }

        public void setSubjectName(String subjectName) {
            this.subjectName = subjectName;
        }

        public String getTeacherName() {
            return teacherName;
        }

        public void setTeacherName(String teacherName) {
            this.teacherName = teacherName;
        }

        public Set<StudentExam> getApplications() {
            return applications;
        }

        public void setApplications(Set<StudentExam> applications) {
            this.applications = applications;
        }

        public ExamDTOWithNames() {
        }

        public ExamDTOWithNames(Exam exam) {

            this.id = exam.getId();
            this.archived = exam.isArchived();
            this.dateTime = exam.getDateTime();
            this.name = exam.getName();
//            this.subjectName = exam.getSubject().getId();
//            this.teacherId = exam.getTeacher().getId();
            this.applications = exam.getApplications();
        }

    }

