package administracija.studenata.project.dto;

import administracija.studenata.project.enums.Role;
import administracija.studenata.project.enums.TeacherRole;
import administracija.studenata.project.model.Exam;
import administracija.studenata.project.model.Subject;
import administracija.studenata.project.model.Teacher;

import java.util.Set;

public class TeacherDTO extends UserDTO {

    private String address;
    private int mobilePhone;
    private Role role;
    private TeacherRole teacherRole;
    private boolean archived;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(int mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public TeacherRole getTeacherRole() {
        return teacherRole;
    }

    public void setTeacherRole(TeacherRole teacherRole) {
        this.teacherRole = teacherRole;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public TeacherDTO(){
        super(); // Poziva prazan konstruktor osnovne klase UserDTO
    }

    public TeacherDTO(Teacher teacher) {

        super(teacher.getId(), teacher.getUsername(), teacher.getName(), teacher.getLastname()); // Poziv konstruktora osnovne klase

        this.address = teacher.getAddress();
        this.mobilePhone = teacher.getMobilePhone();
        this.role = teacher.getRole();
        this.teacherRole = teacher.getTeacherRole();
        this.archived = teacher.isArchived();
    }
}
