package administracija.studenata.project.dto;

import administracija.studenata.project.model.Exam;
import administracija.studenata.project.model.Student;
import administracija.studenata.project.model.Subject;
import administracija.studenata.project.model.Teacher;

import java.util.Set;

public class SubjectDTO {

    private Long id;
    private String name;
    private int ects;
    private boolean archived;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEcts() {
        return ects;
    }

    public void setEcts(int ects) {
        this.ects = ects;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public SubjectDTO(Subject subject){
        this.id = subject.getId();
        this.name = subject.getName();
        this.ects = subject.getEcts();
        this.archived = subject.isArchived();
    }

    public SubjectDTO(){
    }

}
