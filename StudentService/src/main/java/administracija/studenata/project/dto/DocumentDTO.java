package administracija.studenata.project.dto;

import administracija.studenata.project.model.Document;
import administracija.studenata.project.model.Student;

public class DocumentDTO {

    private Long id;
    private String name;
    private String content;
    private boolean archived;
    private Long studentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudent(Long studentId) {
        this.studentId = studentId;
    }

    public DocumentDTO() {
    }

    public DocumentDTO(Document document) {

        this.id = document.getId();
        this.name = document.getName();
        this.archived = document.isArchived();
        this.content = document.getContent();
        this.studentId = document.getStudent().getId();
    }
}
