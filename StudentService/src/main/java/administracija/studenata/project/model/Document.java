package administracija.studenata.project.model;

import administracija.studenata.project.dto.DocumentDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

@Entity
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String content; // TODO: make this contain actual content/location to it on OS.
    @Column(nullable = false)
    private boolean archived;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    @JsonIgnore
    private Student student;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Document (){

    }

    public Document(Long id, String name, String content, Student student) {
        this.id = id;
        this.name = name;
        this.content = content;
        this.student = student;
        this.archived = false;
    }

    public Document(DocumentDTO documentDTO){
        this.name = documentDTO.getName();
        this.content = documentDTO.getContent();
        this.archived = false;
    }

}
