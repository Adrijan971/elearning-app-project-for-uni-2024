package administracija.studenata.project.model;

import administracija.studenata.project.dto.StudentExamDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.time.LocalDateTime;

@Table(name = "exam_application")
@Entity
public class StudentExam {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;
    @Column(nullable = false)
    private LocalDateTime applicationDate;
    private boolean passed;
    private int grade;
    @Column(nullable = false)
    private boolean archived;
    @ManyToOne
    @JoinColumn(nullable = false)
    @JsonIgnore
    private Student student;

    @ManyToOne
    @JoinColumn(nullable = false)
    @JsonIgnore
    private Exam exam;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(LocalDateTime applicationDate) {
        this.applicationDate = applicationDate;
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public int getGrade() {return grade;}

    public void setGrade(int grade) {this.grade = grade;}

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public StudentExam() {
    }

    public StudentExam(Long id, LocalDateTime applicationDate, boolean passed) {
        this.id = id;
        this.applicationDate = applicationDate;
        this.passed = passed;
        this.archived = false;
    }

    public StudentExam(StudentExamDTO studentExamDTO){
        this.archived = false;
        this.applicationDate = studentExamDTO.getApplicationDate();
        this.passed = studentExamDTO.isPassed();
        this.grade = studentExamDTO.getGrade();
    }
}
