package administracija.studenata.project.model;

import administracija.studenata.project.dto.StudentDTO;
import administracija.studenata.project.enums.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Set;

@Entity
public class Student extends User{

    private String bankAccount;
    private double accountBalance;
    @Column(nullable = false)
    private int indexNumber;

    @OneToMany(mappedBy = "student",fetch = FetchType.LAZY)
    private Set<Document> documents;
    @OneToMany(mappedBy = "student",fetch = FetchType.LAZY)
    private Set<Payment> payments;

    //mappedBy does not make sense in many-to-many relation but if there were no mappedBy it would create
    // 2 link tables instead of 1.
    @ManyToMany(mappedBy = "students",fetch = FetchType.LAZY)
    private Set<Subject> subjects;

    @OneToMany(mappedBy = "student",fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<StudentExam> applications;

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public int getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(int indexNumber) {
        this.indexNumber = indexNumber;
    }

    public Set<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(Set<Document> documents) {
        this.documents = documents;
    }

    public Set<Payment> getPayments() {
        return payments;
    }

    public void setPayments(Set<Payment> payments) {
        this.payments = payments;
    }

    public Set<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }

    public Set<StudentExam> getApplications() {
        return applications;
    }

    public void setApplications(Set<StudentExam> applications) {
        this.applications = applications;
    }

    public Student(){

    }

    public Student(Long id, String username, String password, String name, String lastname, String address, int mobilePhone, Role role, float accountBalance, int indexNumber, String bankAccount) {
        super(id,username,password,name,lastname,address,mobilePhone,role);
        this.accountBalance = accountBalance;
        this.bankAccount = bankAccount;
        this.indexNumber = indexNumber;
    }

    public Student(StudentDTO studentDTO){
        super(studentDTO.getId(), studentDTO.getUsername(), "",
                studentDTO.getName(), studentDTO.getLastname(), studentDTO.getAddress(), studentDTO.getMobilePhone(),
                studentDTO.getRole()
        );
        this.indexNumber = studentDTO.getIndexNumber();
        this.bankAccount = studentDTO.getBankAccount();
        this.accountBalance = studentDTO.getAccountBalance();
    }

}
