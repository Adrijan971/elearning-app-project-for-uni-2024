package administracija.studenata.project.model;

import administracija.studenata.project.dto.SubjectDTO;
import administracija.studenata.project.dto.TeacherDTO;
import administracija.studenata.project.enums.Role;
import administracija.studenata.project.enums.TeacherRole;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;

import java.util.Set;

@Entity
public class Teacher extends User{

    //mappedBy does not make sense in many-to-many relation but if there were no mappedBy it would create
    // 2 link tables instead of 1.
    @ManyToMany(mappedBy = "teachers")
    private Set<Subject> subjects;

    @OneToMany(mappedBy = "teacher")
    @JsonIgnore
    private Set<Exam> exams;

    @Column(nullable = false)
    private TeacherRole teacherRole;

    public Set<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }

    public Set<Exam> getExams() {
        return exams;
    }

    public void setExams(Set<Exam> exams) {
        this.exams = exams;
    }

    public TeacherRole getTeacherRole() {
        return teacherRole;
    }

    public void setTeacherRole(TeacherRole teacherRole) {
        this.teacherRole = teacherRole;
    }

    public Teacher(Long id, String username, String password, String name, String lastname, String address, int mobilePhone, Role role) {
        super(id,username,password,name,lastname,address,mobilePhone,role);
    }

    public Teacher(){
    }

    public Teacher(TeacherDTO teacherDTO){
        super(teacherDTO.getId(), teacherDTO.getUsername(), "",
                teacherDTO.getName(), teacherDTO.getLastname(), teacherDTO.getAddress(), teacherDTO.getMobilePhone(),
                teacherDTO.getRole()
        );
    }

}
