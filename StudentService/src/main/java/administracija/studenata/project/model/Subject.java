package administracija.studenata.project.model;

import administracija.studenata.project.dto.SubjectDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Set;

@Entity
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private int ects;
    @Column(nullable = false)
    private boolean archived;

    @ManyToMany
    private Set<Teacher> teachers;

    @ManyToMany
    private Set<Student> students;

    @OneToMany(mappedBy = "subject")
    @JsonIgnore
    private Set<Exam> exams;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEcts() {
        return ects;
    }

    public void setEcts(int ects) {
        this.ects = ects;
    }

    public Set<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(Set<Teacher> teachers) {
        this.teachers = teachers;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public Set<Exam> getExams() {
        return exams;
    }

    public void setExams(Set<Exam> exams) {
        this.exams = exams;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Subject() {
    }

    public Subject(Long id, String name, int ects) {
        this.id = id;
        this.name = name;
        this.ects = ects;
        this.archived = false;
    }

    // TODO dodati ovde jos atributa
    public Subject(SubjectDTO subjectDTO){
        this.name = subjectDTO.getName();
        this.ects = subjectDTO.getEcts();
        this.archived = false;
    }
}
