package administracija.studenata.project.model;

import administracija.studenata.project.dto.AdministratorDTO;
import administracija.studenata.project.dto.StudentDTO;
import administracija.studenata.project.enums.Role;
import jakarta.persistence.Entity;

@Entity
public class Administrator extends User{

    public Administrator() {
    }

    public Administrator(Long id, String username, String password, String name, String lastname, String address, int mobilePhone, Role role) {
        super(id,username,password,name,lastname,address,mobilePhone,role);
    }

    public Administrator(AdministratorDTO administratorDTO){
        super(administratorDTO.getId(), administratorDTO.getUsername(), "",
                administratorDTO.getName(), administratorDTO.getLastname(), administratorDTO.getAddress(), administratorDTO.getMobilePhone(),
                administratorDTO.getRole()
        );
    }
}
