package administracija.studenata.project.model;

import administracija.studenata.project.dto.ExamDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.Set;

@Entity
public class Exam {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;
    @Column(nullable = false)
    private LocalDateTime dateTime;
    @Column(nullable = false)
    private boolean archived;
    @Column(nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(nullable = false)
    @JsonIgnore
    private Subject subject;

    @ManyToOne
    @JoinColumn(nullable = false)
    @JsonIgnore
    private Teacher teacher;

    @OneToMany(mappedBy = "exam")
    @JsonIgnore
    private Set<StudentExam> applications;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Set<StudentExam> getApplications() {
        return applications;
    }

    public void setApplications(Set<StudentExam> applications) {
        this.applications = applications;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Exam() {
    }

    public Exam(Long id, LocalDateTime dateTime, String name) {
        this.id = id;
        this.dateTime = dateTime;
        this.archived = false;
        this.name = name;
    }

    public Exam(ExamDTO examDTO){
        this.archived = false;
        this.dateTime = examDTO.getDateTime();
        this.name = examDTO.getName();
    }
}