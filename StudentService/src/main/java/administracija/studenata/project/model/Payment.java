package administracija.studenata.project.model;

import administracija.studenata.project.dto.PaymentDTO;
import jakarta.persistence.*;

import java.time.LocalDateTime;

@Entity
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;
    @Column(nullable = false)
    private double amount;

    @Column(nullable = false)
    private String bankAccount;

    @Column(nullable = false)
    private LocalDateTime dateTime;

    @Column(nullable = false)
    private boolean archived;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Student student;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Payment() {
    }

    public Payment(Long id, double amount, String bankAccount, LocalDateTime dateTime, Student student) {
        this.id = id;
        this.amount = amount;
        this.bankAccount = bankAccount;
        this.dateTime = dateTime;
        this.student = student;
        this.archived = false;
    }

    public Payment(PaymentDTO paymentDTO){
        this.amount = paymentDTO.getAmount();
        this.bankAccount = paymentDTO.getBankAccount();
        this.dateTime = paymentDTO.getDateTime();
        this.archived = false;
    }

}
