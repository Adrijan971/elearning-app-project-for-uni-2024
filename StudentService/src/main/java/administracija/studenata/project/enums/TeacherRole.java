package administracija.studenata.project.enums;

public enum TeacherRole {
    DEMONSTRATOR, ASSISTANT, PROFESSOR
}
