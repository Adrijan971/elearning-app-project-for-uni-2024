package administracija.studenata.project.enums;

public enum Role {
    STUDENT, TEACHER, ADMINISTRATOR
}
