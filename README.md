# Student Service project for Uni 2024 Spring Boot Java


## Database scheme
![test](project_requirement/database-scheme.PNG)

## Project specification

Technologies and Systems of eEducation(class name) - Project Assignment

Implement a software application for managing the teaching process in an educational institution. The application should support the administration of:

- Students
- Teachers
- Courses
- Course attendance
- Course teaching – a course can be taught by different teachers with various roles (e.g., lecturer, assistant, demonstrator, etc.)
- Student documents – each student has a set of electronic documents associated with them (forms submitted during enrollment, diploma, etc.)
- Student payments
- Exams – all exams, quizzes, and other academic obligations performed by students during their courses must be recorded
- Application users – the application is accessed by administrators, teachers, and students

**User roles**:
  - Administrators have access to all parts of the application.
  - Teachers manage their own data and the data related to the courses they are involved in.
  - Students can view their related information, such as courses they attend, a list of passed exams, their payment history, and documents.


**Application Implementation**

The application should be implemented as a client-server web application:

  - Server-side:
    - Use the Java platform and the Spring framework.
Implement data persistence using a relational database with object-relational mapping.
Provide a REST API for communication.
  - Client-side:
    - Develop a Single Page Application (SPA) using standard web client technologies and the Angular framework.
